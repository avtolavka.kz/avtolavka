package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.ClientAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ClientAddressRepository extends JpaRepository<ClientAddress, Long> {
}
