package kz.attractor.datamodel.model;

import java.util.HashMap;
import java.util.Map;

public enum ProductStatus {

    IN_STOCK("В наличии"),
    NOT_AVAILABLE("Под заказ");

    public final String name;
    private static final Map<String, ProductStatus> BY_NAME = new HashMap<>();

    static {
        for(ProductStatus status: values()) {
            BY_NAME.put(status.name, status);
        }
    }
    private ProductStatus(String name) {
        this.name = name;
    }

    public static ProductStatus valueOfLabel(String name) {
        return BY_NAME.get(name);
    }
}
