package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DocumentRepository extends JpaRepository<Document, Long>, JpaSpecificationExecutor<Document> {
    Page<Document> findAll(Pageable pageable);
    Page<Document> findAll(Specification<Document> specification, Pageable pageable);
    List<Document> findAll(Specification<Document> specification);
}
