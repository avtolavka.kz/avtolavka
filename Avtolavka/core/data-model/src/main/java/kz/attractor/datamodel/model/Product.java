package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(name = "article", columnDefinition = "varchar(50000)" )
    private String article;

    @Column(name = "articleSearch", columnDefinition = "varchar(50000)")
    private String articleSearch;


    @Column(name = "name", columnDefinition="text")
    private String name;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "dateEdit")
    private String dateEdit;

    @Column(name = "unit", columnDefinition="text")
    private String unit;

    @Column(name = "purchase_price")
    private BigDecimal purchasePrice;

    @Column(name = "selling_price")
    private BigDecimal sellingPrice;


    @Column(name = "in_stock")
    private ProductStatus status;

    @Column(name = "quality")
    private ProductQuality quality;


    @Column(name = "tag", columnDefinition="varchar(5000)")
    private String tag;


    @Column(name = "image", columnDefinition="varchar(5000)")
    private String image;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "productName_id")
    private ProductName productName;


    @Column(name = "manufacturer", columnDefinition="varchar(5000)")
    private String manufacturer;


    @Column(name = "description", columnDefinition="varchar(5000)")
    private String description;
}
