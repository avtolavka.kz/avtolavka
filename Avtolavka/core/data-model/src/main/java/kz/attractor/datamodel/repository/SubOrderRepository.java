package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.SubOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubOrderRepository extends JpaRepository<SubOrder, Long> {
}
