package kz.attractor.datamodel.model;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_names")
@Builder
public class ProductName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nameProduct", columnDefinition="text")
    private String nameProduct;

    @Column(name = "nameProduct2", columnDefinition="text")
    private String nameProduct2;
    @Column(name = "nameProduct3", columnDefinition="text")
    private String nameProduct3;
    @Column(name = "nameProduct4", columnDefinition="text")
    private String nameProduct4;
}
