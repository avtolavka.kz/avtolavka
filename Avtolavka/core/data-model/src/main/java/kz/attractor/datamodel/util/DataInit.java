package kz.attractor.datamodel.util;

import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
@AllArgsConstructor
public class DataInit {
    private final WarehouseRepository warehouseRepository;
    private final ClientKbeRepository clientKbeRepository;
    private final UserRepository userRepository;
    private final DataInitExcelParseProfile dataInitExcelParseProfile;
    private final AccountPaymentStatusRepository accountPaymentStatusRepository;
    private final AccountSupplyStatusRepository accountSupplyStatusRepository;
    private final CompanyRepository companyRepository;


    @Bean
    public CommandLineRunner init() {
        try {
            initClientKbe().run();
            initWarehouses().run();
            initUsers().run();
            initAccountPaymentStatus().run();
            initAccountSupplyStatus().run();
            initCompany().run();
            dataInitExcelParseProfile.initExcelParseProfiles().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private CommandLineRunner initUsers() {
        return (args -> Stream.of(users())
                .peek(System.out::println)
                .forEach(userRepository::save));
    }

    private CommandLineRunner initWarehouses() {
        return (args -> Stream.of(warehouses())
                .peek(System.out::println)
                .forEach(warehouseRepository::save));
    }

    private CommandLineRunner initClientKbe() {
        return (args) -> Stream.of(clientKbes())
                .peek(System.out::println)
                .forEach(clientKbeRepository::save);
    }
    private CommandLineRunner initAccountPaymentStatus() {
        return (args) -> Stream.of(accountPaymentStatuses())
                .peek(System.out::println)
                .forEach(accountPaymentStatusRepository::save);
    }

    private CommandLineRunner initAccountSupplyStatus() {
        return (args) -> Stream.of(accountSupplyStatuses())
                .peek(System.out::println)
                .forEach(accountSupplyStatusRepository::save);
    }

    private CommandLineRunner initCompany() {
        return (args) -> Stream.of(companies())
                .peek(System.out::println)
                .forEach(companyRepository::save);
    }


    private User[] users() {
        return new User[]{
                new User(1L, "Ирина Гордеева", "admin.admin", "$2a$12$VWSBTnMbOL80Bwoq7OCzduMSa/XWCMw6VSMK/jPUZkMJVT2URorWS", "Директор", "87075825070", "ADMIN", true)
        };
    }

    private Company[] companies(){
        return new Company[]{
                new Company(1L, "191040025113", "ТОО Авто-Лавка.кзт", 17, 751210000, 57050609,
                        "с 22.10.2019г Серия 60001 №1209975", "АО Народный Банк Казахстана", "HSBKKZKX", "KZ736018861000694801 (KZT)",
                        "г.Алматы, ул.Утеген Батыра, дом-11А", "г.Алматы, ул.Утеген Батыра, дом-11А, офис 107","50031" ,
                        "buh@auto-lavka.kz", "87027033023", "105 Малые предприятия (<=5)", "47191 Прочая розничная торговля в неспециализированных",
                        "zakup@auto-lavka.kz", "Гордеева Ирина Сергеевна", "87075825070"),
                new Company(2L, "810521400890", "ИП РТИ-экспертKAZ", 19, 0,0 ,"", "АО Народный Банк Казахстана", "HSBKKZKX", "KZ436018861000624531 (KZT)",
                        "обл. Алматинская, г.Алматы, ул.Райымбека, ТЦ Барлык, А-30, 20","г.Алматы, мкр.Аксай-1 А, дом-1, офис3/19", "50031",
                        "buh@auto-lavka.kz", "87027033023", "105 Малые предприятия (<=5)", "47191 Прочая розничная торговля в неспециализированных 45310 Оптовая торговля автомобильными деталями, узлами и принадлежностями 45200 Техобслуживание и ремонт автотранспортных средств. 49710 Грузовые перевозки автомобильным транспортом и услуги по перевозкам",
                        "info@auto-lavka.kz", "Гордеева Ирина Сергеевна","87075825070" ),
                new Company(3L, "740515302199", "ИП ЕГАЙ", 19 , 0,0, "", "АО Народный Банк Казахстана", "HSBKKZKX", "KZ66601A861001250691 (KZT)",
                        "обл.Алматинская г.Алматы, Рынок Жибек-Жолы, 8-ряд, 40 место", "г.Алматы, мкр.Аксай-1А, дом-1, офис3/19", "50031",
                        "buh@auto-lavka.kz", "87027968076", "105 Малые предприятия (<=5)", "47191 Прочая розничная торговля в неспециализированных",
                        "info@auto-lavka.kz", "Егай Сергей Анатольевич", "87017202531")
        };
    }

    private Warehouse[] warehouses() {
        return new Warehouse[]{
                new Warehouse(1L, "Склад в Алматы", true, "3 дня", "Алматы"),
                new Warehouse(2L, "Price Stock Shymkent Khamdamov - GAZ", true, "9 дней", "PSSH-G"),
                new Warehouse(3L, "Нет склада", false, "", "")
        };
    }
    private AccountSupplyStatus[] accountSupplyStatuses(){
        return new AccountSupplyStatus[]{
                new AccountSupplyStatus(1L, "Поставлен" , 100),
                new AccountSupplyStatus(2L, "Не поставлен", 0),
                new AccountSupplyStatus(3L, "Поставлен частично", 0)
        };
    }
    private AccountPaymentStatus[] accountPaymentStatuses(){
        return new AccountPaymentStatus[]{
                new AccountPaymentStatus(1L, "Оплачено" , 100),
                new AccountPaymentStatus(2L, "Не оплочено", 0),
                new AccountPaymentStatus(3L, "Оплочено частично", 0)
        };
    }
    private ClientKbe[] clientKbes() {
        return new ClientKbe[]{
                new ClientKbe(1L, "Товарищество с ограниченной ответственностью", "ТОО", 17),
                new ClientKbe(2L, "Индивидуальный предприниматель", "ИП", 19),
                new ClientKbe(3L, "Акционерное общество", "АО", 17),
                new ClientKbe(4L, "", "", 0)
        };
    }


}
