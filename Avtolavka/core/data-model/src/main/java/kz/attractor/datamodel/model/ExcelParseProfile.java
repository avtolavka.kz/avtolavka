package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "excel_parse_profile")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExcelParseProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "profile_name")
    private String profileName;

    @Column(name = "row_start")
    private Integer rowStart;

    @Column(name = "column_name")
    private Integer colName;

    @Column(name = "column_article")
    private Integer colArticle;

    @Column(name = "column_price")
    private Integer colPrice;

    @Column(name = "column_quantity")
    private Integer colQuantity;

    @Column(name = "manufacturer")
    private Integer colManufacturer;

    @Column(name = "column_unit")
    private Integer colUnit;

    @Column(name = "default_quantity")
    private Integer defaultQuantity;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;
}
