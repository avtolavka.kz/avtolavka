package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.ExcelParseProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExcelParseProfileRepository extends JpaRepository<ExcelParseProfile, Long> {}
