package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.AccountPaymentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountPaymentStatusRepository extends JpaRepository<AccountPaymentStatus, Long> {
}
