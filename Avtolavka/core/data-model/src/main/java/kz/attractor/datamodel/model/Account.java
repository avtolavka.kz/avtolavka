package kz.attractor.datamodel.model;

import lombok.*;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;



@Getter
@Setter
@Entity
@Table(name = "accounts")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String guid;

    private String number;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    private BigDecimal sum;

    @Column(name = "is_borrow")
    private boolean isBorrow;

    @OneToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @OneToOne
    @JoinColumn(name = "payment_id")
    private AccountPaymentStatus paymentStatus;

    @OneToOne
    @JoinColumn(name = "supply_id")
    private AccountSupplyStatus supplyStatus;

    @Column(name = "file")
    private String file;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

//    @Column(name = "isFile")
//    private boolean isFile;
}
