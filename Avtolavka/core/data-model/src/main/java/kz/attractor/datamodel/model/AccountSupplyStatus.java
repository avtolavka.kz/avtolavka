package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "supply_status")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountSupplyStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "sum_supply")
    private double sumSupply;
}
