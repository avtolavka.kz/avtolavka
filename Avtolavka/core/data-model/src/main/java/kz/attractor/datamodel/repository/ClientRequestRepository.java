package kz.attractor.datamodel.repository;

import kz.attractor.datamodel.model.ClientRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRequestRepository extends JpaRepository<ClientRequest, Long> {

}
