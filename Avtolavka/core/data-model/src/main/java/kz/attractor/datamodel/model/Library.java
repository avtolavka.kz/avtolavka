package kz.attractor.datamodel.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "library")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String title;
    @Column(name = "subtitle")
    private String subtitle;
    @Column(name = "dateCreate")
    private String date;
    @Column(name = "descriptionPromo", columnDefinition="text")
    private String descriptionPromo;
    @Column(name = "imagePromo")
    private String imagePromo;
    @Column(name = "image")
    private String image;
    @Column(name = "description", columnDefinition="text")
    private String description;

    @Column(name = "file")
    private String file;
}
