package kz.attractor.datamodel.model;

import java.util.HashMap;
import java.util.Map;

public enum TaskPriority {

    TASK_CURRENT("Текущий"),
    TASK_MEDIUM("Средний"),
    TASK_HIGH("Высокий");

    public final String label;
    private static final Map<String, TaskPriority> BY_LABEL = new HashMap<>();

    static {
        for(TaskPriority status: values()) {
            BY_LABEL.put(status.label, status);
        }
    }
    private TaskPriority(String label) {
        this.label = label;
    }

    public static TaskPriority valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
