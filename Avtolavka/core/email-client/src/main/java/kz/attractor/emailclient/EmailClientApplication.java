package kz.attractor.emailclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "kz.attractor")
public class EmailClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailClientApplication.class, args);
    }

}
