package kz.attractor.emailclient.service;

import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Map;

public interface EmailService {
    void sendMessageAsText(String to, String subject, String text);

    void sendMessageAsHtml(String to, String subject, String templatePath, Map<String, Object> maps) throws MessagingException, IOException, TemplateException;

    void sendMessageWithAttachment(String to, String subject, String text, String attachmentLocation, String attachmentDescription) throws MessagingException;
}