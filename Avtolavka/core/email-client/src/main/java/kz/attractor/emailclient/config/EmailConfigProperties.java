package kz.attractor.emailclient.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "email")
@PropertySource("classpath:email.properties")
@Data
public class EmailConfigProperties {
    private String host;
    private int port;
    private String username;
    private String password;
    private String senderEmail;
}
