package kz.attractor.common.exception;

public class InternalServiceError extends RuntimeException {
    public InternalServiceError(String message) {
        super(message);
    }
}
