package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.InvoiceDto;
import kz.attractor.api.service.InvoiceService;
import kz.attractor.api.service.OrderService;
import kz.attractor.datamodel.model.OrderProduct;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/invoices")
public class InvoiceApiController {
    private final InvoiceService invoiceService;
    private final OrderService orderService;

    @GetMapping("/{id}")
    public HashMap<String, Object> findInvoice(@PathVariable long id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        InvoiceDto invoice = invoiceService.findById(id);
        Long orderId = invoice.getOrder().getId();
        hashMap.put("invoice", invoice);
        List<OrderProduct> orderProducts = orderService.findOrderProductsByOrderId(orderId);
        hashMap.put("orderProducts", orderProducts);
        return hashMap;
    }
}