package kz.attractor.api.dto;

import kz.attractor.api.config.Param;
import kz.attractor.datamodel.model.*;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private int id;
    private String name;
    private String article;
    private String articleSearch;
    private int quantity;
    private String dateEdit;
    private BigDecimal purchasePrice;
    private BigDecimal sellingPrice;
    private BigDecimal price;
    private String status;
    private String quality;
    private String image;
    private Warehouse warehouse;
    private ProductName productName;
    private String tag;
    private String unit;
    private String description;
    private String manufacturer;

    public String getStrId(){
        return String.valueOf(id);
    }

    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .article(product.getArticle())
                .articleSearch(product.getArticleSearch())
                .quantity(product.getQuantity())
                .dateEdit(product.getDateEdit())
                .purchasePrice(product.getPurchasePrice())
                .sellingPrice(product.getSellingPrice())
                .price(BigDecimal.valueOf(Math.round(product.getPurchasePrice()
                        .multiply(BigDecimal.valueOf(1 + Param.RATE_FIRST))
                        .multiply(BigDecimal.valueOf(1 + Param.RATE_SECOND))
                        .doubleValue())))
                .status(product.getStatus().name)
                .unit(product.getUnit())
                .quality(product.getQuality().label)
                .image(product.getImage())
                .warehouse(product.getWarehouse())
                .productName(product.getProductName())
                .tag(product.getTag())
                .manufacturer(product.getManufacturer())
                .description(product.getDescription())
                .build();
    }
}
