package kz.attractor.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderDtoAdd {
    private long orderId;
    private int contactId;
    private List<ProductDto> products;
    private int companyId;
    private long userId;
}
