package kz.attractor.api.service;

import kz.attractor.api.dto.AccountDtoAdd;
import kz.attractor.api.dto.InvoiceDto;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final OrderRepository orderRepository;
    private final OrderProductRepository orderProductRepository;
    private final AccountPaymentStatusRepository accountPaymentStatusRepository;
    private final AccountSupplyStatusRepository accountSupplyStatusRepository;

    public void add(AccountDtoAdd accountDtoAdd) {
        Order order = orderRepository.getById(accountDtoAdd.getOrderId());
        AccountPaymentStatus accountPaymentStatus = accountPaymentStatusRepository.getById(2L);
        AccountSupplyStatus accountSupplyStatus = accountSupplyStatusRepository.getById(2L);
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderId(order.getId());
        BigDecimal sum = orderProducts.stream()
                        .map(i -> i.getPrice().multiply(BigDecimal.valueOf(i.getQuantity())))
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
        Account account = Account.builder()
                .number(order.getClient().getAccountNumber())
                .guid(UUID.randomUUID().toString())
                .createdDate(LocalDateTime.now())
                .isBorrow(accountDtoAdd.getBorrow().equals("true")?true:false)
                .sum(sum)
                .order(order)
                .paymentStatus(accountPaymentStatus)
                .supplyStatus(accountSupplyStatus)
                .file(accountDtoAdd.getFile())
//                .isFile(accountDtoAdd.getFile().equals("true")?true:false)
                .build();

        accountRepository.save(account);
        System.out.println(account);
    }

    public List<InvoiceDto> findAll() {
        var invoices = accountRepository.findAll()
                .stream()
                .map(InvoiceDto::from)
                .collect(Collectors.toList());
        return invoices;
    }
}
