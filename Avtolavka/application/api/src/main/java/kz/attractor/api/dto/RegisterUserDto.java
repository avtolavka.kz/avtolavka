package kz.attractor.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserDto {
    @Size(min = 4, max = 70, message = "Имя пользователя должно состоять от 4 до 70 символов")
    @NotBlank(message = "Имя пользователя не может быть пустой")
    private String username;

    @Size(min = 4, max = 70, message = "Полное имя должно состоять от 4 до 70 символов")
    @NotBlank(message = "Полное имя не может быть пустой")
    private String fullName;

    @Size(min = 6, max = 20, message = "Пароль должен состоять от 6 до 20 символов")
    @NotBlank(message = "Пароль не может быть пустым")
    private String password;

    @Size(min = 10, max = 12, message = "Номер телефона должен состоять от 10 до 12 символов")
    @NotBlank(message = "Номер телефона не может быть пустым")
    private String phone;
}
