package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.*;
import kz.attractor.api.service.WarehouseService;
import kz.attractor.datamodel.model.Product;
import kz.attractor.datamodel.model.Warehouse;
import kz.attractor.datamodel.repository.ProductRepository;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class WarehouseController {
    private final WarehouseService warehouseService;
    private final WarehouseRepository warehouseRepository;
    private final ProductRepository productRepository;

    @GetMapping("/warehouses")
    public String showWarehouses(Model model,
                              @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        Page<WarehouseDto> warehouses = warehouseService.findAll(pageable);
        model.addAttribute("page", warehouses);
        return "warehouses";
    }


    @GetMapping("/warehouses/{id}")
    public String showWarehouse(@PathVariable long id, Model model) {
        WarehouseDto warehouse = warehouseService.findById(id);
        model.addAttribute("warehouse", warehouse);
        return "warehouse";
    }

    @GetMapping("/warehouses/{id}/edit")
    public String update(@PathVariable long id, Model model) {
        WarehouseDto warehouses = warehouseService.findById(id);
        model.addAttribute("form", warehouses);
        return "warehouse-edit";
    }

    @PostMapping("/warehouse-edit")
    public String edit( @Valid WarehouseDto form,
                        BindingResult validationResult,
                        RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/warehouses/" + form.getId() + "/warehouse-edit";
        }
        warehouseService.update(form);
        return "redirect:/warehouses";
    }

    @GetMapping("/warehouses/add")
    public String add() {return "warehouses-add";}

    @PostMapping("warehouse-add")
    public String add(@Valid WarehouseAddDto form,
                      BindingResult validationResult,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/add";
        }
        warehouseService.add(form);
        return "redirect:/warehouses";
    }

    @GetMapping("/warehouses/{id}/delete")
    public String delete(@PathVariable long id, Model model){
        WarehouseDto warehouseDto = warehouseService.findById(id);
        model.addAttribute("warehouse", warehouseDto);
        List<Product> products = productRepository.findAllByWarehouse_Id(id);
        if (products.isEmpty()){
            return "warehouse-delete";
        }
        return "warehouse-delete-error";
    }

    @PostMapping("/warehouse-delete")
    public String delete(@RequestParam long id){
        Warehouse warehouse = warehouseRepository.getById(id);
        List<Product> products = productRepository.findAllByWarehouse_Id(id);
        if (products.isEmpty()){
            warehouseService.delete(warehouse);
            return "redirect:/warehouses";
        }
        return "warehouse-delete-error";
    }
}
