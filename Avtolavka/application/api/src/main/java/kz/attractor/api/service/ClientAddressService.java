package kz.attractor.api.service;

import kz.attractor.api.dto.ClientAddressAddDto;
import kz.attractor.api.dto.ClientAddressDto;
import kz.attractor.datamodel.model.ClientAddress;
import kz.attractor.datamodel.repository.ClientAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClientAddressService {

    private final ClientAddressRepository clientAddressRepository;

    public ClientAddress add (ClientAddressAddDto form){
        String physicalCountry = form.getPhysical_country().isEmpty()? form.getLegal_country() : form.getPhysical_country();
        String physicalCity = form.getPhysical_city().isEmpty()? form.getLegal_city() : form.getPhysical_city();
        String physicalStreet = form.getPhysical_street().isEmpty()? form.getLegal_street() : form.getPhysical_street();
        String physicalHouse = form.getPhysical_house().isEmpty() ? form.getLegal_house() : form.getPhysical_house();
        ClientAddress clientAddress = ClientAddress.builder()
                .legal_country(form.getLegal_country())
                .legal_city(form.getLegal_city())
                .legal_street(form.getLegal_street())
                .legal_house(form.getLegal_house())
                .physical_country(physicalCountry)
                .physical_city(physicalCity)
                .physical_street(physicalStreet)
                .physical_house(physicalHouse)
                .build();

        return clientAddressRepository.save(clientAddress);
    }

    public ClientAddress update(ClientAddressDto form){
        var clientAddressOpt = clientAddressRepository.findById(form.getId());
        if (clientAddressOpt.isEmpty()) {
            return null;
        }
        ClientAddress clientAddress = clientAddressOpt.get();
        clientAddress.setLegal_country(form.getLegal_country());
        clientAddress.setLegal_city(form.getLegal_city());
        clientAddress.setLegal_street(form.getLegal_street());
        clientAddress.setLegal_house(form.getLegal_house());
        clientAddress.setPhysical_country(form.getPhysical_country());
        clientAddress.setPhysical_city(form.getPhysical_city());
        clientAddress.setPhysical_street(form.getPhysical_street());
        clientAddress.setPhysical_house(form.getPhysical_house());

        return clientAddressRepository.save(clientAddress);
    }

}