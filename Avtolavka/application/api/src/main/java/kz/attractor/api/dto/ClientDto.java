package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Client;
import kz.attractor.datamodel.model.ClientBank;
import kz.attractor.datamodel.model.ClientKbe;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {
    private Long id;

    @NotBlank(message = "Поле не должно быть пустым")
    private String name;
    private String nameUpdate;

    private String resident;

    @NotBlank(message = "Поле не должно быть пустым")
    private String shortName;
    private String shortNameUpdate;

    @NotBlank(message = "Поле не должно быть пустым")
    private String accountNumber;

    @NotBlank(message = "Поле не должно быть пустым")
    private String bin;

    @NotBlank(message = "Поле не должно быть пустым")
    private ClientKbe clientKbe;

    @NotBlank(message = "Поле не должно быть пустым")
    private ClientAddressDto address;
    private String file;
    private Integer kbe;
    private String phoneMain;
    private String phone1;
    private String phone2;
    private String phone3;
    private String emailMain;
    private String email1;
    private String email2;
    private String email3;
    private String status;
    private ClientBank bank;
    private String createDate;

    public static ClientDto from(Client client) {
        return ClientDto.builder()
                .id(client.getId())
                .nameUpdate(client.getName())
                .name( client.getName())
                .resident(client.getResident())
                .shortName(client.getShortName())
                .shortNameUpdate(client.getShortName())
                .accountNumber(client.getAccountNumber())
                .address(ClientAddressDto.from(client.getAddress()))
                .clientKbe(client.getClientKbe())
                .kbe(client.getClientKbe().getKbe())
                .bin(client.getBin())
                .file(client.getFile())
                .phoneMain(client.getPhoneMain())
                .phone1(client.getPhone1())
                .phone2(client.getPhone2())
                .phone3(client.getPhone3())
                .emailMain(client.getEmailMain())
                .email1(client.getEmail1())
                .email2(client.getEmail2())
                .email3(client.getEmail3())
                .status(client.getStatus().label)
                .bank(client.getBank())
                .createDate(client.getCreateDate())
                .build();
    }
}