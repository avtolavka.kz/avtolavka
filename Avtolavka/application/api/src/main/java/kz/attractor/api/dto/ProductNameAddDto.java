package kz.attractor.api.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductNameAddDto {

    @NotBlank(message = "Поле не должно быть пустым")
    private String nameProduct;

    private String nameProduct2;
    private String nameProduct3;
    private String nameProduct4;
}
