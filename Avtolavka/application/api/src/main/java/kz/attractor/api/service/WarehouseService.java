package kz.attractor.api.service;

import kz.attractor.api.dto.WarehouseAddDto;
import kz.attractor.api.dto.WarehouseDto;
import kz.attractor.datamodel.model.Warehouse;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class WarehouseService {
    private final WarehouseRepository warehouseRepository;

    public void add(WarehouseAddDto form){
        Warehouse warehouse = Warehouse.builder()
                .name(form.getName())
                .shortName(form.getShortName())
                .deliveryTime(form.getDeliveryTime())
                .includeToPriceList(true)
                .build();
        warehouseRepository.save(warehouse);
    }

    public WarehouseDto update(WarehouseDto form){
        var warehouseOptional = warehouseRepository.findById(form.getId());
        if (warehouseOptional.isEmpty()){
            return null;
        }
        Warehouse warehouse = warehouseOptional.get();
        warehouse.setName(form.getName());
        warehouse.setShortName(form.getShortName());
        warehouse.setDeliveryTime(form.getDeliveryTime());

        return WarehouseDto.from(warehouseRepository.save(warehouse));
    }

    public WarehouseDto findById(long id) {
        var warehouse = warehouseRepository.findById(id).orElse(null);
        return WarehouseDto.from(warehouse);
    }

    public Page<WarehouseDto> findAll(Pageable pageable) {
        Page<Warehouse> warehouses = warehouseRepository.findAll(pageable);
        return new PageImpl<WarehouseDto>(
                warehouses.getContent().stream()
                        .map(WarehouseDto::from)
                        .collect(Collectors.toList()),
                pageable, warehouses.getTotalElements()
        );
    }

    public void delete(Warehouse warehouse){
        warehouseRepository.delete(warehouse);
    }
}
