package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.*;
import kz.attractor.api.service.ProductService;
import kz.attractor.api.service.PropertiesService;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.OrderProductRepository;
import kz.attractor.datamodel.repository.ProductNameRepository;
import kz.attractor.datamodel.repository.ProductRepository;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@AllArgsConstructor
public class ProductController {
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;
    private final WarehouseRepository warehouseRepository;
    private final ProductNameRepository productNameRepository;
    private final OrderProductRepository orderProductRepository;
    private final PropertiesService propertiesService;




    @GetMapping("/products")
    public String showProducts(Model model,
                                   @RequestParam(required = false) String query,
                                   @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, size = 20) Pageable pageable,
                                   HttpServletRequest uriBuilder) {
        Page<ProductDto> products = productService.findAll(query, pageable);
        var uri  = uriBuilder.getRequestURI();
        constructPageable(products, propertiesService.getDefaultPageSize(), model, uri);
        var productsPage = productService.findAll(query, pageable);
        model.addAttribute("page", productService.findAll(query, pageable));
        if(query != null) {
            model.addAttribute("link", query);
        }
        return "products";
    }

    @PostMapping("/products")
    public String searchProducts(@RequestParam String query, Model model,
                                 @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, size = 20) Pageable pageable) {
        model.addAttribute("products", productService.findAll(query, pageable));
        return "products";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri,
                            list.nextPageable().getPageNumber(),
                            list.nextPageable().getPageSize())
            );
        }
        if (list.hasPrevious()) {
            model.addAttribute("nextPreviousLink",
                    constructPageUri(uri,
                            list.previousPageable().getPageNumber(),
                            list.previousPageable().getPageSize())
            );
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrevious", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        if (uri.contains("?")) {
            return String.format("%s&page=%s&size=%s", uri, page, size);
        }
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }



    @GetMapping("products/add")
    public String add(Model model){
        List<Warehouse> warehouses = warehouseRepository.findAll();
        model.addAttribute("warehouses", warehouses );
        return "product-add";}

    @PostMapping("product-add")
    public String add(@Valid ProductDtoAdd form,
                      @Valid ProductNameAddDto formName,
                      BindingResult validationResult,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        attributes.addFlashAttribute("formName", formName);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/products/add";
        }

        productService.add(form, formName);
        return "redirect:/products";
    }

    @GetMapping("product/{id}/edit")
    public String update(@PathVariable int id, Model model){
        ProductDto product = productService.findById(id);
        ProductName productName = productNameRepository.findById((long) id).get();
        List<Warehouse> warehouses = warehouseRepository.findAll();
        model.addAttribute("formName", productName);
        model.addAttribute("form", product);
        model.addAttribute("warehouses", warehouses);
        model.addAttribute("status", ProductStatus.values());
        return "product-edit";
    }

    @PostMapping("product-edit")
    public String edit(@Valid ProductDto form,
                       @Valid ProductNameDto formName,
                       BindingResult validationResult,
                       RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        attributes.addFlashAttribute("formName", formName);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/products/" + form.getId() + "/edit";
        }
        productService.update(form, formName);
        return "redirect:/products/";
    }

    @GetMapping("/products/{id}/productQuality")
    public String getProductQuality(@PathVariable int id, Model model){
        Product product = productRepository.getById(id);
        model.addAttribute("product", product);
        model.addAttribute("quality", ProductQuality.values());
        return "productQuality";
    }

    @PostMapping("/productQuality")
    public String addQuality(@RequestParam int id, @RequestParam String quality){
        productService.addQuality(id, quality);
        return "redirect:/products";
    }

//    @GetMapping("/products/")
//    public String product(@RequestParam long id, Model model){
//        ProductDto product = productService.findById(id);
//        model.addAttribute("product", product);
//        return "product";
//    }
}
