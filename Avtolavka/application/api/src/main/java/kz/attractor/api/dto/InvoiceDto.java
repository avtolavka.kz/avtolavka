package kz.attractor.api.dto;

import kz.attractor.datamodel.model.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDto {
    private Long id;
    private String guid;
    private String number;
    private LocalDateTime createdDate;
    private BigDecimal sum;
    private boolean isBorrow;
    private Order order;
    private AccountPaymentStatus accountPaymentStatus;
    private AccountSupplyStatus supplyStatus;
    private String file;
    private boolean isFile;

    public static InvoiceDto from(Account account) {
        return InvoiceDto.builder()
                .id(account.getId())
                .guid(account.getGuid())
                .number(account.getNumber())
                .createdDate(account.getCreatedDate().truncatedTo(ChronoUnit.SECONDS))
                .sum(account.getSum())
                .isBorrow(account.isBorrow())
                .order(account.getOrder())
                .accountPaymentStatus(account.getPaymentStatus())
                .supplyStatus(account.getSupplyStatus())
                .file(account.getFile())
//                .isFile(account.isFile())
                .build();
    }
}