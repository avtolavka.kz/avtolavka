package kz.attractor.api.controller.apiController;

import freemarker.template.TemplateException;
import kz.attractor.api.price.PriceService;
import kz.attractor.api.service.ClientService;
import kz.attractor.api.service.EmailService;
import kz.attractor.api.service.OrderService;
import kz.attractor.common.enums.PriceFileType;
import kz.attractor.common.enums.SenderType;
import kz.attractor.datamodel.model.Order;
import kz.attractor.datamodel.model.OrderProduct;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/email")
@AllArgsConstructor
public class EmailApiController {
    private final EmailService emailService;
    private final OrderService orderService;
    private final PriceService priceService;
    private final ClientService clientService;

    @GetMapping("/order/{id}")
    public Map<String, Object> sendOrderAsHtml(@PathVariable Long id) throws MessagingException, TemplateException, IOException {
        Order order = orderService.findById(id);
        List<OrderProduct> products = orderService.findOrderProductsByOrderId(id);
        Map<String, Object> maps = new HashMap<>();
        maps.put("order", order);
        maps.put("products", products);
        emailService.sentEmailAsHtml(order.getClient().getEmailMain(), "price",
                "/email/email-order.ftlh", maps);
        return maps;
    }


    @PostMapping("/clients/send_price")
    @ResponseStatus(HttpStatus.OK)
    public void sendPrice(@RequestParam(value = "type", defaultValue = "EXCEL") String type, @RequestBody List<Long> messagingClientIds) {
        priceService.sendPrice(PriceFileType.valueOf(type), SenderType.MAIL, messagingClientIds);
    }
}

