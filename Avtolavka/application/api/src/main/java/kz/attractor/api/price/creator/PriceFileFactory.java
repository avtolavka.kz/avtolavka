package kz.attractor.api.price.creator;

import kz.attractor.api.dto.ProductDto;
import kz.attractor.common.enums.PriceFileType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class PriceFileFactory {

    public static final HashMap<PriceFileType, PriceCreator<ProductDto, File>> priceFileCreators = new HashMap<>();


    static {
        priceFileCreators.put(PriceFileType.EXCEL, new PriceExcelFileCreator());
        priceFileCreators.put(PriceFileType.CSV, new PriceCsvFileCreator());
    }
}
