package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Contact;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactDto {
    private long id;
    private String name;
    private String phoneMain;
    private String phone1;
    private String phone2;
    private String phone3;
    private String emailMain;
    private String email1;
    private String email2;
    private String email3;
    private String position;
    private String status;
    private long clientId;
    private String clientName;
    private String date;
    private String dateOfBirth;

    public static ContactDto from (Contact contact) {
        return ContactDto.builder()
                .id(contact.getId())
                .name(contact.getName())
                .phoneMain(contact.getPhoneMain())
                .phone1(contact.getPhone1())
                .phone2(contact.getPhone2())
                .phone3(contact.getPhone3())
                .emailMain(contact.getEmailMain())
                .email1(contact.getEmail1())
                .email2(contact.getEmail2())
                .email3(contact.getEmail3())
                .position(contact.getPosition())
                .status(contact.getStatus().label)
                .clientId(contact.getClient().getId())
                .clientName(contact.getClient().getShortName())
                .date(contact.getDate())
                .dateOfBirth(contact.getDateOfBirth())
                .build();
    }
}
