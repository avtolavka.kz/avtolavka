package kz.attractor.api.price.creator;

import kz.attractor.api.dto.ProductDto;
import kz.attractor.common.exception.InternalServiceError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


@Component
@Slf4j
@RequiredArgsConstructor
public class PriceExcelFileCreator implements PriceCreator<ProductDto, File> {


    @Override
    public File create(List<ProductDto> products) {
        try {
            File file = createFile();
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet spreadsheet = workbook.createSheet(" Student Data ");
            Map<String, Object[]> values = new TreeMap<>();
            addHeader(values);
            for (int i = 0; i < products.size(); i++) {
                values.put(String.valueOf(i + 2), objectToRow(products.get(i)));
            }
            Set<String> keyId = values.keySet();
            int rowId = 0;
            for (String key : keyId) {
                XSSFRow row = spreadsheet.createRow(rowId++);
                Object[] objectArr = values.get(key);
                int cellId = 0;
                for (Object obj : objectArr) {
                    Cell cell = row.createCell(cellId++);
                    cell.setCellValue((String) obj);
                    spreadsheet.autoSizeColumn(cell.getRow().getPhysicalNumberOfCells());
                }
            }
            return writeToFile(workbook, file);
        } catch (Exception e) {
            throw new InternalServiceError(String.format("Не удалось создать прайс : %s", e.getMessage()));
        }

    }

    private File writeToFile(XSSFWorkbook workbook, File file) {
        log.info("Формирование файла: {}", file.getName());
        try (var out = new FileOutputStream(file)) {
            workbook.write(out);
        } catch (IOException e) {
            log.error("Ошибка при записи в файл : {}", file.getName());
        }
        return file;
    }

    private void addHeader(Map<String, Object[]> values) {
        values.put("1", new Object[]{
                "Название",
                "Артикул",
                "Цена",
                "Количество",
                "Склад"
        });
    }

    private Object[] objectToRow(ProductDto productDto) {
        return new Object[]{
                productDto.getName(),
                productDto.getArticle(),
                String.valueOf(productDto.getPrice()),
                String.valueOf(productDto.getQuantity()),
                productDto.getWarehouse().getName()
        };
    }

    private File createFile() {
        try {
            Path dir = Files.createDirectories(Paths.get("prices/"));
            Path filePath = Files.createFile(dir.resolve("price.xlsx"));
            return filePath.toFile();
        } catch (Exception e) {
            throw new InternalServiceError(String.format("Не удалось создать файл для прайса : %s", e.getMessage()));
        }
    }
}
