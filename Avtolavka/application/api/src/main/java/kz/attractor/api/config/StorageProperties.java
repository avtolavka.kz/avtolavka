package kz.attractor.api.config;

import lombok.Data;

import java.nio.file.Path;

@Data
public class StorageProperties {
    private String location;


    public Path getPathLocation() {
        return Path.of(location);
    }
}