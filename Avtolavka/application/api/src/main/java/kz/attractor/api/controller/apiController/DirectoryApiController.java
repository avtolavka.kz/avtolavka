package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.HandbookDto;
import kz.attractor.api.service.DirectoryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/directories")
public class DirectoryApiController {
    private final DirectoryService service;

    @GetMapping("{id}")
    public HandbookDto getDirectoryById(@PathVariable int id) {
        return service.findById(id);
    }
}
