package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.ExcelParseProfileAddDto;
import kz.attractor.api.dto.ExcelParseProfileDto;
import kz.attractor.api.dto.FileStoryDto;
import kz.attractor.api.service.ExcelParseProfileService;
import kz.attractor.api.service.ExcelParseService;
import kz.attractor.api.service.FileStoryService;
import kz.attractor.datamodel.model.Warehouse;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/excel")
public class ExcelParseController {
    private final ExcelParseService excelParseService;
    private final ExcelParseProfileService excelParseProfileService;
    private final FileStoryService fileStoryService;
    private final WarehouseRepository warehouseRepository;

    @GetMapping()
    public String selectExcel(Model model) {
        List<ExcelParseProfileDto> profiles = excelParseService.findAllExcelParseProfiles();
        model.addAttribute("profiles", profiles);
        List<FileStoryDto> stories = fileStoryService.findAll();
        model.addAttribute("stories", stories);
        return "excel-parse";
    }

    @PostMapping("/parse")
    public String parseExcel(@RequestParam MultipartFile multipartFile,
                             @RequestParam long profileId) throws IOException {
        if (!multipartFile.isEmpty()) {
            excelParseService.parseExcel(multipartFile, profileId);
        }
        return "redirect:/products";
    }

    @GetMapping("/add-profile")
    public String addProfile(Model model) {
        List<ExcelParseProfileDto> profiles = excelParseService.findAllExcelParseProfiles();
        List<Warehouse> warehouses = warehouseRepository.findAll();
        model.addAttribute("profiles", profiles);
        model.addAttribute("warehouses", warehouses);
        return "excel-add-profile";
    }

    @PostMapping("/add-profile")
    public String addProfile(ExcelParseProfileAddDto form) {
        System.out.println(form);
        excelParseProfileService.add(form);
        return "redirect:/excel";
    }
}
