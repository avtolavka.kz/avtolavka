package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.ClientKbeDto;
import kz.attractor.api.service.ClientKbeService;
import kz.attractor.datamodel.model.ClientKbe;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/clientKbes")
public class ClientKbeApiController {

    private final ClientKbeService clientKbeService;

    @GetMapping()
    public List<ClientKbeDto> getClientKbes() {
        return clientKbeService.findAll();

    }
}
