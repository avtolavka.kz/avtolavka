package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.DirectoryAddDto;
import kz.attractor.api.dto.HandbookDto;
import kz.attractor.api.service.DirectoryService;
import kz.attractor.api.service.PropertiesService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class DirectoryController {
    private final DirectoryService directoryService;
    private final PropertiesService propertiesService;


    @GetMapping("/directories")
    public String showLibraryPage(Model model,
                                  @PageableDefault(sort = {"article"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable,
                                  HttpServletRequest uriBuilder) {
        Page<HandbookDto> handbooks = directoryService.findAll(pageable);
        var uri  = uriBuilder.getRequestURI();
        constructPageable(handbooks, propertiesService.getDefaultPageSize(), model, uri);
        model.addAttribute("page", handbooks);
        return "directories";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink",
                    constructPageUri(uri,
                            list.nextPageable().getPageNumber(),
                            list.nextPageable().getPageSize())
            );
        }
        if (list.hasPrevious()) {
            model.addAttribute("nextPreviousLink",
                    constructPageUri(uri,
                            list.previousPageable().getPageNumber(),
                            list.previousPageable().getPageSize())
            );
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrevious", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        if (uri.contains("?")) {
            return String.format("%s&page=%s&size=%s", uri, page, size);
        }
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping("/directories/{id}")
    public String showLibraries(@PathVariable long id, Model model) {
        HandbookDto directory = directoryService.findById(id);
        model.addAttribute("directory", directory);
        return "directory";
    }

    @GetMapping("/directories/{id}/edit")
    public String edit(@PathVariable long id, Model model) {
        HandbookDto directory = directoryService.findById(id);
        model.addAttribute("form", directory);
        return "directory-edit";
    }

    @PostMapping("directory-edit")
    public String edit(@Valid HandbookDto form,
                       BindingResult validationResult,
                       RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/" + form.getId() + "/edit";
        }
        directoryService.update(form);
        return "redirect:/directories";
    }

    @GetMapping("/directory/add")
    public String add() {
        return "directory-add";
    }

    @PostMapping("directory-add")
    public String add(@Valid DirectoryAddDto form,
                      BindingResult validationResult,
                      RedirectAttributes attributes) {
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/directory-add";
        }
        directoryService.add(form);
        return "redirect:/directories";
    }


}
