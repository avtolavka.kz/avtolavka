package kz.attractor.api.service;

import kz.attractor.api.dto.ProductNameAddDto;
import kz.attractor.api.dto.ProductNameDto;
import kz.attractor.datamodel.model.ProductName;
import kz.attractor.datamodel.repository.ProductNameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductNameService {

    private final ProductNameRepository productNameRepository;

    public ProductName add(ProductNameAddDto form){
        ProductName productName = ProductName.builder()
                .nameProduct(form.getNameProduct())
                .nameProduct2(form.getNameProduct2())
                .nameProduct3(form.getNameProduct3())
                .nameProduct4(form.getNameProduct4())
                .build();
        return productNameRepository.save(productName);

    }

    public ProductName update(ProductNameDto form){
        var productOpt = productNameRepository.findById(form.getId());
        if (productOpt.isEmpty()) {
            return null;
        }
        ProductName productName = ProductName.builder()
                .nameProduct(form.getNameProduct())
                .nameProduct2(form.getNameProduct2())
                .nameProduct3(form.getNameProduct3())
                .nameProduct4(form.getNameProduct4())
                .build();

       return productNameRepository.save(productName);
    }
}