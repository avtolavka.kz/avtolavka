package kz.attractor.api.controller.apiController;

import kz.attractor.api.dto.AccountDtoAdd;
import kz.attractor.api.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class AccountApiController {

    private final AccountService accountService;

    @PostMapping("/api/accounts/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAccount(AccountDtoAdd accountDtoAdd) {
        accountService.add(accountDtoAdd);
    }
}
