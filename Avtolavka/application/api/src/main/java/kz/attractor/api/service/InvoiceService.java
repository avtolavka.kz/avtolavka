package kz.attractor.api.service;

import kz.attractor.api.dto.InvoiceDto;
import kz.attractor.datamodel.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InvoiceService {
    private final AccountRepository invoiceRepository;

    public InvoiceDto findById(long id) {
        var invoice = invoiceRepository.findById(id).get();
        return InvoiceDto.from(invoice);
    }
}
