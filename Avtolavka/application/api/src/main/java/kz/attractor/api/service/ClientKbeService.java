package kz.attractor.api.service;

import kz.attractor.api.dto.ClientKbeAddDto;
import kz.attractor.api.dto.ClientKbeDto;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.ClientKbe;
import kz.attractor.datamodel.repository.ClientKbeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClientKbeService {
    private final ClientKbeRepository clientKbeRepository;

    public List<ClientKbeDto> findAll() {
        var clientKbes = clientKbeRepository.findAll()
                .stream()
                .map(ClientKbeDto::from)
                .collect(Collectors.toList());
        clientKbes.sort(Comparator.comparing(ClientKbeDto::getShortName).reversed());
        return clientKbes;
    }
    public ClientKbeDto findById(long id) {
        var clientKbe = clientKbeRepository.findById(id).orElseThrow( () ->
                new EntityMissingException(ClientKbe.class, id));
        return ClientKbeDto.from(clientKbe);
    }

    public ClientKbeDto add(ClientKbeAddDto form){
        ClientKbe clientKbe = ClientKbe.builder()
                .name(form.getName())
                .shortName(form.getShortName())
                .kbe(form.getKbe())
                .build();

        ClientKbe clientKbeSaved = clientKbeRepository.save(clientKbe);
        return ClientKbeDto.from(clientKbeSaved);
    }

}
