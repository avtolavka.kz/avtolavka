package kz.attractor.api.service;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailService {
    private final JavaMailSender mailSender;

    @Autowired
    private Configuration configuration;

//    private final EmailClient emailClient;

    public void sentEmailAsText(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("anita.gus@yandex.kz");
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            mailSender.send(message);
        } catch (Exception e) {
            log.error("Ошибка отправки сообщения", e);
        }
    }

    public void sentEmailAsHtml(String to,
                                String subject,
                                String templatePath,
                                Map<String, Object> maps) throws MessagingException, TemplateException, IOException {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
            helper.setTo(to);
            message.setFrom("anita.gus@yandex.kz");
            helper.setSubject(subject);
            helper.setText(getText(templatePath, maps), true);
            mailSender.send(message);
        } catch (Exception e) {
            log.error("Ошибка отправки сообщения", e);
        }
    }

    public void sentEmailWithAttachment(String to,
                                        String subject,
                                        String text,
                                        String attachmentLocation,
                                        String attachmentDescription) {
        try {
            log.info("Отправка сообщения : {} {} ,", to, subject);
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            message.setFrom("anita.gus@yandex.kz");
            helper.setSubject(subject);
            helper.setText(text);
            FileDataSource file = new FileDataSource(attachmentLocation);
            helper.addAttachment(attachmentDescription, file);
            mailSender.send(message);
        } catch (MessagingException e) {
            log.error("Не удалось отправить сообщение {}", e.getMessage());
        }
    }

    private String getText(String templatePath, Map<String, Object> maps) throws IOException, TemplateException {
        var template = configuration.getTemplate(templatePath);
        var text = FreeMarkerTemplateUtils.processTemplateIntoString(template, maps);
        return text;
    }
}

