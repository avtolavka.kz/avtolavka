package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ClientAddress;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientAddressDto{
    private long id;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_country;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_city;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_street;

    @NotBlank(message = "Поле не должно быть пустым")
    private String legal_house;

    private String physical_country;

    private String physical_city;

    private String physical_street;

    private String physical_house;


    public static ClientAddressDto from(ClientAddress clientAddress) {
        return ClientAddressDto.builder()
                .id(clientAddress.getId())
                .legal_country(clientAddress.getLegal_country())
                .legal_city(clientAddress.getLegal_city())
                .legal_street(clientAddress.getLegal_street())
                .legal_house(clientAddress.getLegal_house())
                .physical_country(clientAddress.getPhysical_country())
                .physical_city(clientAddress.getPhysical_city())
                .physical_street(clientAddress.getPhysical_street())
                .physical_house(clientAddress.getPhysical_house())
                .build();
    }
}