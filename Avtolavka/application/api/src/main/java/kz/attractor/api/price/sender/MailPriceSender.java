package kz.attractor.api.price.sender;

import kz.attractor.datamodel.model.Client;
import kz.attractor.emailclient.service.EmailClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailPriceSender implements PriceSender<EmailClient> {

    public void send(File file, Client client, EmailClient emailClient) {
        log.info("Отправка - прайса: ({}) , почта:  ({})", file.getName(), client.getEmailMain());
        emailClient.sendMessageWithAttachment(client.getEmailMain(), "Прайс", "", file.getAbsolutePath(), "price.xlsx");
    }
}
