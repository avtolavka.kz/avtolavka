package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Handbook;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HandbookDto {

    private long id;
    private String title;
    private String article;
    private String name;
    private String name2;
    private String name3;
    private String name4;

    public static HandbookDto from(Handbook directory){
        return HandbookDto.builder()
                .id(directory.getId())
                .title(directory.getTitle())
                .article(directory.getArticle())
                .name(directory.getName())
                .name2(directory.getName2())
                .name3(directory.getName3())
                .name4(directory.getName4())
                .build();
    }
}
