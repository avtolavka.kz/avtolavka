package kz.attractor.api.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileStoryDtoAdd {
    @NotBlank(message = "Поле не должно быть пустым")
    private String name;
    private String createDate;
}
