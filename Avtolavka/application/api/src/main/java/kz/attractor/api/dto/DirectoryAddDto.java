package kz.attractor.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DirectoryAddDto {
    @NotBlank(message = "Поле не должно быть пустым")
    private String title;
    @NotBlank(message = "Поле не должно быть пустым")
    private String article;
    @NotBlank(message = "Поле не должно быть пустым")
    private String name;
    private String name2;
    private String name3;
    private String name4;
}
