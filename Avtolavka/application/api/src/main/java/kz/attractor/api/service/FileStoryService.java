package kz.attractor.api.service;


import kz.attractor.api.dto.FileStoryDto;
import kz.attractor.datamodel.model.FileStory;
import kz.attractor.datamodel.repository.FileStoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FileStoryService {
    private final FileStoryRepository fileStoryRepository;

    public Page<FileStoryDto> findAll(Pageable pageable) {
        Page<FileStory> fileStories = fileStoryRepository.findAll(pageable);
        return new PageImpl<FileStoryDto>(
                fileStories.getContent().stream()
                        .map(FileStoryDto::from)
                        .sorted(Comparator.comparing(FileStoryDto::getId).reversed())
                        .collect(Collectors.toList()),
                pageable, fileStories.getTotalElements()
        );
    }

    public List<FileStoryDto> findAll() {
        var fileStories = fileStoryRepository.findAll()
                .stream()
                .map(FileStoryDto::from)
                .collect(Collectors.toList());
        fileStories.sort(Comparator.comparing(FileStoryDto::getId));
        return fileStories;
    }

    public void add(String name){
        FileStory fileStory = new FileStory();
        fileStory.setName(name);
        fileStory.setCreateDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        fileStoryRepository.save(fileStory);
    }
}
