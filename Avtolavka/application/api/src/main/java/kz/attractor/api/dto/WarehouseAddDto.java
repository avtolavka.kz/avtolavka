package kz.attractor.api.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseAddDto {
    private String name;
    private boolean includeToPriceList;
    private String deliveryTime;
    private String shortName;
}
