package kz.attractor.api.service;

import kz.attractor.api.dto.RegisterUserDto;
import kz.attractor.api.dto.UserDto;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.datamodel.model.User;
import kz.attractor.datamodel.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public List<UserDto> findAll() {
        List<UserDto> users = userRepository.findAll().stream()
                .map(UserDto::from)
                .collect(Collectors.toList());
        return users;
    }

    public UserDto findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new EntityMissingException(User.class, id));
        return UserDto.from(user);
    }

    public boolean register(RegisterUserDto userDTO) {
        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user != null) {
            return false;
        }

        user = User.builder()
                .username(userDTO.getUsername())
                .fullname(userDTO.getFullName())
                .position("Нет позиций")
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .phone(userDTO.getPhone())
                .role("UNKNOWN")
                .enabled(true)
                .build();
        userRepository.save(user);
        return true;
    }

    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    public void changeRole(long userId, String role) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if(!optionalUser.isPresent()) {
            return;
        }
        User user = optionalUser.get();
        user.setRole(role);
        userRepository.save(user);
    }
}
