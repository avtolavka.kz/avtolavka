package kz.attractor.api.service;

import kz.attractor.api.dto.LibraryAddDto;
import kz.attractor.api.dto.LibraryDto;
import kz.attractor.api.dto.ProductDto;
import kz.attractor.datamodel.model.Library;
import kz.attractor.datamodel.model.LibrarySpecification;
import kz.attractor.datamodel.model.Product;
import kz.attractor.datamodel.model.ProductSpecification;
import kz.attractor.datamodel.repository.LibraryRepository;
import kz.attractor.datamodel.util.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LibraryService {
    private final LibraryRepository libraryRepository;

    public Page<LibraryDto> findAll(String query, Pageable pageable) {
        if (query != null) {
            LibrarySpecification name = new LibrarySpecification(new SearchCriteria("title", ":", query));
            LibrarySpecification description = new LibrarySpecification(new SearchCriteria("description", ":", query));
            Page<Library> libraryPage = libraryRepository.findAll(Specification.where(name).or(description), pageable);
            return new PageImpl<LibraryDto>(
                    libraryPage.getContent().stream()
                            .map(LibraryDto::from)
                            .collect(Collectors.toList()),
                    pageable, libraryPage.getTotalElements()
            );
        }
        Page<Library> libraryPage = libraryRepository.findAll(pageable);
        return new PageImpl<LibraryDto>(
                libraryPage.getContent().stream()
                        .map(LibraryDto::from)
                        .collect(Collectors.toList()),
                pageable, libraryPage.getTotalElements()
        );
    }

    public Page<LibraryDto> findAll(Pageable pageable) {
        Page<Library> libraries = libraryRepository.findAll(pageable);
        return new PageImpl<LibraryDto>(
                libraries.getContent().stream()
                        .map(LibraryDto::from)
                        .sorted(Comparator.comparing(LibraryDto::getDate).reversed())
                        .collect(Collectors.toList()),
                pageable, libraries.getTotalElements()
        );
    }

    public LibraryDto findById(long id) {
        var library = libraryRepository.findById(id).orElse(null);
        return LibraryDto.from(library);
    }

    public void add(LibraryAddDto form){
        Library library = Library.builder()
                .title(form.getTitle())
                .subtitle(form.getSubtitle())
                .date(LocalDate.now().toString())
                .description(form.getDescription())
                .imagePromo(form.getImagePromo())
                .descriptionPromo(form.getDescriptionPromo())
                .image(form.getImage())
                .build();
        libraryRepository.save(library);
    }

    public LibraryDto update(LibraryDto form){
        var libraryOptional = libraryRepository.findById(form.getId());
        if (libraryOptional.isEmpty()){
            return null;
        }
        Library library = libraryOptional.get();
        library.setTitle(form.getTitle());
        library.setSubtitle(form.getSubtitle());
        library.setDate(form.getDate());
        library.setDescriptionPromo(form.getDescriptionPromo());
        library.setImagePromo(form.getImagePromo());
        library.setDescription(form.getDescription());
        library.setImage(form.getImage());
        return LibraryDto.from(libraryRepository.save(library));
    }

    public void delete(LibraryDto form){
        var libraryOptional = libraryRepository.findById(form.getId());
        Library library = libraryOptional.get();
        libraryRepository.delete(library);
    }
}
