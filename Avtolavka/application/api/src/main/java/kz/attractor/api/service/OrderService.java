package kz.attractor.api.service;

import kz.attractor.api.dto.*;
import kz.attractor.common.exception.EntityMissingException;
import kz.attractor.common.exception.UserNotFoundException;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderProductRepository orderProductRepository;
    private final ContactRepository contactRepository;
    private final ProductRepository productRepository;
    private final AccountRepository accountRepository;
    private final CompanyRepository companyRepository;
    private final SubOrderRepository subOrderRepository;
    private final ClientRepository clientRepository;
    private final UserRepository userRepository;

    public Page<OrderDto> findAll(Pageable pageable) {
        Page<Order> ordersPage = orderRepository.findAll(pageable);
        return new PageImpl<OrderDto>(
                ordersPage.getContent().stream()
                        .map(OrderDto::from)
                        .collect(Collectors.toList()),
                pageable, ordersPage.getTotalElements()
        );
    }

    public void add(OrderDtoAdd form) {
        Client client = clientRepository.findById((long)form.getContactId()).get();
//        User user = userRepository.findByUsername(authentication.getName()).orElseThrow(UserNotFoundException::new);
        Company company = companyRepository.findById((long)form.getCompanyId()).get();
        Order order = Order.builder()
                .dateCreation(LocalDateTime.now())
                .client(client)
                .isClosed(false)
                .hasBonus(true)
                .company(company)
                .build();
        orderRepository.save(order);

        for(ProductDto productDto : form.getProducts()) {
            Product product = productRepository.findById(productDto.getId()).get();
            OrderProduct orderProduct = OrderProduct.builder()
                    .product(product)
                    .order(order)
                    .quantity(productDto.getQuantity())
                    .price(productDto.getPrice())
                    .unit(productDto.getUnit())
                    .sum(productDto.getPrice().multiply(new BigDecimal(productDto.getQuantity())).doubleValue())
                    .build();
            orderProductRepository.save(orderProduct);
        }
    }

    public Order findById(Long id) {
        return orderRepository.findById(id).orElseThrow( () ->
                new EntityMissingException(Order.class, id));
    }

    public List<OrderProduct> findOrderProductsByOrderId(Long id) {
        return orderProductRepository.findAllByOrderId(id);
    }

    public void closeStatus(long orderId) {
        Order order = orderRepository.findById(orderId).get();
        order.setClosed(true);
        orderRepository.save(order);
    }

    public void edit(OrderDtoAdd form) {
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderId(form.getOrderId());

        for(OrderProduct orderProduct : orderProducts) {
            boolean hasProduct = false;
            for(ProductDto productDto : form.getProducts()) {
                if (orderProduct.getProduct().getId() == productDto.getId()) {
                    orderProduct.setQuantity(productDto.getQuantity());
                    orderProduct.setUnit(productDto.getUnit());
                    orderProductRepository.save(orderProduct);
                    hasProduct = true;
                }
            }
            if (!hasProduct) {
                orderProductRepository.delete(orderProduct);
            }
        }
    }

    public void handleBonus(long orderId, boolean bonus) {
        Order order = orderRepository.findById(orderId).get();
        order.setHasBonus(bonus);
        orderRepository.save(order);
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderId(orderId);
        if(bonus) {
            for (int i = 0; i < orderProducts.size(); i++) {
                orderProducts.get(i).setPrice(orderProducts.get(i).getProduct().getSellingPrice());
                orderProductRepository.save(orderProducts.get(i));
            }
        } else {
            for (int i = 0; i < orderProducts.size(); i++) {
                BigDecimal bonusPercent = new BigDecimal(12);
                BigDecimal bonusValue = orderProducts.get(i).getProduct().getSellingPrice().multiply(bonusPercent.divide(new BigDecimal(100)));
                orderProducts.get(i).setPrice(orderProducts.get(i).getProduct().getSellingPrice().subtract(bonusValue));
                orderProductRepository.save(orderProducts.get(i));
            }
        }
    }
    public void updateSupplyStatus(long accountId, InvoiceDto form){
        Account account = accountRepository.getById(accountId);
        account.setSupplyStatus(form.getSupplyStatus());
        accountRepository.save(account);
    }

    public InvoiceDto updatePaymentStatus(long id, InvoiceDto form){
       Account account= accountRepository.getById(id);
        account.setPaymentStatus(form.getAccountPaymentStatus());
        return InvoiceDto.from(accountRepository.save(account));
    }

    public void addSuborder(long parentOrderId, long company ) {
        Order parentOrder = orderRepository.findById(parentOrderId).orElse(null);
        Client client = clientRepository.findById(parentOrderId).get();
        Company company1 = companyRepository.findById(company).orElse(null);
        OrderProduct orderProduct = orderProductRepository.findById(parentOrderId).orElse(null);
        Order order = Order.builder()
                .dateCreation(LocalDateTime.now())
                .client(client)
                .isClosed(false)
                .hasBonus(true)
                .company(company1)
                .build();
        orderRepository.save(order);

        SubOrder subOrder = new SubOrder();
        subOrder.setOrder(parentOrder);
        subOrder.setOrder(order);
        subOrder.setOrderProduct(orderProduct);
        subOrderRepository.save(subOrder);
        }

}
