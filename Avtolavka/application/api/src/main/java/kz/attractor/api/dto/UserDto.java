package kz.attractor.api.dto;

import kz.attractor.datamodel.model.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserDto {
    private Long id;
    private String fullname;
    private String username;
    private String position;
    private String phone;
    private String role;

    public static UserDto from(User user) {
        return UserDto.builder()
                .id(user.getId())
                .fullname(user.getFullname())
                .username(user.getUsername())
                .position(user.getPosition())
                .phone(user.getPhone())
                .role(user.getRole())
                .build();
    }
}
