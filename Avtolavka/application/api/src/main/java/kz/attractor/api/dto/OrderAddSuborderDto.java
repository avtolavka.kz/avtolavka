package kz.attractor.api.dto;


import lombok.*;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderAddSuborderDto {

    private long orderId;
    private long orderProduct;

}
