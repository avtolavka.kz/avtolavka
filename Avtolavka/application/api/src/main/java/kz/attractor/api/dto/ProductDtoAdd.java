package kz.attractor.api.dto;


import kz.attractor.datamodel.model.ProductName;
import kz.attractor.datamodel.model.ProductStatus;
import kz.attractor.datamodel.model.Unit;
import kz.attractor.datamodel.model.Warehouse;
import lombok.*;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDtoAdd {

    @NotBlank(message = "Поле не должно быть пустым")
    private String name;

    @NotBlank(message = "Поле не должно быть пустым")
    private String article;

    private String articleSearch;

    @Positive
    private int quantity;

    @Positive
    private BigDecimal purchasePrice;

    @Positive
    private BigDecimal sellingPrice;

    private String dateEdit;
    private String unit;
    private String image;

    private ProductStatus status;

    private String tag;

    private Warehouse warehouse;

    private ProductName productName;

    private String description;

    private String manufacturer;
}