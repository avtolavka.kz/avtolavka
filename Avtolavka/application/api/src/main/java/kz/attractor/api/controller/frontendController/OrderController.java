package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.*;
import kz.attractor.api.service.*;
import kz.attractor.api.service.OrderExcelservice.OrderExcelService;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.net.URLEncoder;
import java.util.List;

@Controller
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final AccountService accountService;
    private final ClientService clientService;
    private final ClientRequestService requestService;
    private final AccountPaymentStatusRepository accountPaymentStatusRepository;
    private final AccountSupplyStatusRepository accountSupplyStatusRepository;
    private final AccountRepository accountRepository;
    private final StorageService fileSystemStorageService;
    private final CompanyRepository companyRepository;
    private final OrderProductRepository orderProductRepository;
    private final OrderExcelService orderExcelService;

    @GetMapping("/orders")
    public String showOrdersPage(Model model,
                                 @PageableDefault(sort = {"id"}, direction = Sort.Direction.ASC, size = 10) Pageable pageable){
        Page<OrderDto> orders = orderService.findAll(pageable);
        model.addAttribute("page", orders);
        model.addAttribute("accounts", accountService.findAll());
        model.addAttribute("clients", clientService.findAll());
        return "orders";
    }

    @GetMapping("/orders/add")
    public String showAddOrdersPage(Model model) {
        List<Company> companies = companyRepository.findAll();
        model.addAttribute("requests", requestService.findAll());
        model.addAttribute("companies", companies);
        return "order_add";
    }

    @PostMapping("/orders/close")
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String closeOrderStatus(@RequestParam long orderId) {
        orderService.closeStatus(orderId);
        return "redirect:/orders";
    }
    @GetMapping("/orders/{id}/editPaymentStatus")
    public String editPaymentStatus(Model model, @PathVariable long id){
        Account account = accountRepository.getById(id);
        List<AccountPaymentStatus> accountPaymentStatuses = accountPaymentStatusRepository.findAll();
        model.addAttribute("account", account);
        model.addAttribute("accountPaymentStatuses", accountPaymentStatuses);
        return "editPaymentStatus";
    }

    @GetMapping("/orders/{id}/editSupplyStatus")
    public String editSupplyStatus(Model model, @PathVariable long id){
        Account account = accountRepository.getById(id);
        List<AccountSupplyStatus> accountSupplyStatuses = accountSupplyStatusRepository.findAll();
        model.addAttribute("account", account);
        model.addAttribute("accountSupplyStatuses", accountSupplyStatuses);
        return "editSupplyStatus";
    }

    @PostMapping("/editPaymentStatus")
    public String editPayment(@Valid InvoiceDto form, @RequestParam long id,
                              BindingResult validationResult,
                              RedirectAttributes attributes){
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/orders/" + form.getId() + "/editPaymentStatus";
        }
        orderService.updatePaymentStatus(id, form);
        return "redirect:/orders";
    }

    @PostMapping("/editSupplyStatus")
    public String editSupply(@Valid InvoiceDto form, @RequestParam long id,
                              BindingResult validationResult,
                              RedirectAttributes attributes){
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/orders/" + form.getId() + "/editSupplyStatus";
        }
        orderService.updateSupplyStatus(id, form);
        return "redirect:/orders";
    }

    @GetMapping("/orders/{id}/contract-add")
    public String contractAdd(Model model, @PathVariable long id){
        Account account = accountRepository.getById(id);
        model.addAttribute("account", account);
        return "contract-add";
    }

    @GetMapping("/orders/{id}/suborder")
    public String suborder(Model model, @PathVariable long id){
        Order orderId = orderService.findById(id);
        List<Company> companies = companyRepository.findAll();
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderId(id);
        model.addAttribute("orderProducts", orderProducts);
        model.addAttribute("orderId", orderId);
        model.addAttribute("company", companies);
        return "suborder";
    }

    @PostMapping("/suborder")
    public String suborder(@RequestParam long orderId,@RequestParam long companyId){

        orderService.addSuborder(orderId, companyId);
        return "redirect:/orders";
    }

    @GetMapping("/orders/{id}/download")
    public void outExcel (HttpServletResponse response, @PathVariable long id) throws Exception {
        String exportName = "Order" ;
        response.setContentType("application/vnd.ms-excel");
        response.addHeader("Content-Disposition", "attachment;filename="+
                URLEncoder.encode(exportName, "UTF-8") + ".xlsx");
        String[] headList = new String[]{"Название", "Ед.измерения","Количество", "Цена с НДС", "Итого с НДС"} ;
        orderExcelService.exportExcel(headList,id,response.getOutputStream()) ;


    }
//    @PostMapping("/orders/add/save-request")
//    public String saveOriginalRequest(@Valid ClientRequestDto dto){
//        requestService.save(dto);
//        return "redirect:/orders/add";
//    }
}
