package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.ClientAddressDto;
import kz.attractor.api.dto.ClientDto;
import kz.attractor.api.dto.ClientKbeDto;
import kz.attractor.api.dto.ContactDto;
import kz.attractor.api.service.ClientService;
import kz.attractor.api.service.ContactService;
import kz.attractor.api.service.OrderService;
import kz.attractor.api.service.StorageService;
import kz.attractor.common.exception.StorageFileNotFoundException;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;
    private final ContactService contactService;
    private final ContactRepository contactRepository;
    private final ClientRepository repository;
    private final StorageService fileSystemStorageService;
    private final AccountRepository accountRepository;
    private final ClientKbeRepository clientKbeRepository;
    private final ClientRepository clientRepository;
    private final OrderRepository orderRepository;

    @GetMapping("/clients")
    public String showClients(Model model,
                              @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 20) Pageable pageable) {
        Page<ClientDto> clients = clientService.findAll(pageable);
        model.addAttribute("page", clients);
        return "clients";
    }

    @GetMapping("/clients/{id}")
    public String showClient(@PathVariable long id, Model model) {
        ClientDto client = clientService.findById(id);
        model.addAttribute("client", client);
        List<ContactDto> contacts = contactService.findAllByClientId(client.getId());
        model.addAttribute("contacts", contacts);
        return "client";
    }

    @GetMapping("/clients/{id}/edit")
    public String edit(@PathVariable long id, Model model) {
        ClientDto client = clientService.findById(id);
        List<ClientKbe> clientKbes = clientKbeRepository.findAll();
        ClientAddressDto address = clientService.findById(id).getAddress();
        model.addAttribute("addressForm", address);
        model.addAttribute("form", client);
        model.addAttribute("clientKbes", clientKbes);
        model.addAttribute("banks", ClientBank.values());
        return "client-edit";
    }

    @PostMapping("/client-edit")
    public String edit( @Valid ClientDto form,
                        @Valid ClientAddressDto addressForm,
                        @Valid ClientKbeDto clientKbeDto,
                        BindingResult validationResult,
                        RedirectAttributes attributes) {
        attributes.addFlashAttribute("clientForm", form);
        attributes.addFlashAttribute("addressForm", addressForm);
        attributes.addFlashAttribute("clientKbe", clientKbeDto);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/clients/" + form.getId() + "/client-edit";
        }
        clientService.update(form, addressForm);
        return "redirect:/clients/";
    }

    @GetMapping("/clients/add")
    public String add(Model model) {
        model.addAttribute("banks", ClientBank.values());
        return "client-add";
    }

    @GetMapping("/clients/{id}/document-add")
    public String documentAdd(@PathVariable long id, Model model){
        ClientDto client = clientService.findById(id);
        model.addAttribute("client", client);
        return "document-add";
    }

    @PostMapping("document-add")
    public String documentAdd(@RequestParam long id, @RequestParam("file") MultipartFile file){
        Client client = repository.findById(id).get();
        fileSystemStorageService.store(file);
        client.setFile(file.getOriginalFilename());
        repository.save(client);
        return "clients";
    }

    @PostMapping("/contract-add")
    public String contractAdd(@RequestParam long id, @RequestParam("file") MultipartFile file){
        Account account = accountRepository.getById(id);
        fileSystemStorageService.store(file);
        account.setFile(file.getOriginalFilename());
        accountRepository.save(account);
        return "redirect:/orders";
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename){
        org.springframework.core.io.Resource file = fileSystemStorageService.loadAsResource(filename);
        return ResponseEntity.ok().header(
                        HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename= \"" + file.getFilename() + "\"")
                .body(file);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> fileNotFound(StorageFileNotFoundException e){
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/clients/{id}/client-delete")
    public String delete(@PathVariable long id, Model model){
        ClientDto clientDto = clientService.findById(id);
        model.addAttribute("client", clientDto);
        List<Contact> contacts = contactRepository.findAllByClient_Id(id);
        List<Order> orders = orderRepository.findAllByClientId(id);
        if (contacts.isEmpty() && orders.isEmpty()){
            return "client-delete";
        }
        return "client-delete-error";

    }

    @PostMapping("/client-delete")
    public String delete(@RequestParam long id){
        Client client = clientRepository.getById(id);
        List<Contact> contacts = contactRepository.findAllByClient_Id(id);
        List<Order> orders = orderRepository.findAllByClientId(id);
        if (contacts.isEmpty() && orders.isEmpty()){
            clientService.delete(client);
            return "redirect:/clients";
        }
       return "client-delete-error";
    }

    @PostMapping("/client-archive")
    public String archive(@RequestParam long id){
        Client client = clientRepository.getById(id);
        client.setStatus(ClientStatus.CLIENT_ARCHIVE);
        clientRepository.save(client);
        return "redirect:/clients";
    }
}