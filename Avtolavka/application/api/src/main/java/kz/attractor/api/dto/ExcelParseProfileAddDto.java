package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Warehouse;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExcelParseProfileAddDto {
    private String profileName;
    private Integer rowStart;
    private Integer colName;
    private Integer colManufacturer;
    private Integer colArticle;
    private Integer colPrice;
    private Integer colQuantity;
    private Integer colUnit;
    private Integer defaultQuantity;
    private Warehouse warehouse;
}
