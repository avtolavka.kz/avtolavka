package kz.attractor.api.service;


import kz.attractor.api.dto.DirectoryAddDto;
import kz.attractor.api.dto.HandbookDto;
import kz.attractor.datamodel.model.Handbook;
import kz.attractor.datamodel.model.DirectorySpecification;
import kz.attractor.datamodel.repository.HandbookRepository;
import kz.attractor.datamodel.util.SearchCriteria;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.stream.Collectors;

@Slf4j
@Service
@Getter
@Setter
@RequiredArgsConstructor
public class DirectoryService {
    private final HandbookRepository directoryRepository;

    public Page<HandbookDto> findAll(String query, Pageable pageable) {
        if (query != null) {
            DirectorySpecification article = new DirectorySpecification(new SearchCriteria("article", ":", query));
            DirectorySpecification title = new DirectorySpecification(new SearchCriteria("title", ":", query));
            Page<Handbook> directoryPage = directoryRepository.findAll(Specification.where(article).or(title), pageable);
            return new PageImpl<HandbookDto>(
                    directoryPage.getContent().stream()
                            .map(HandbookDto::from)
                            .collect(Collectors.toList()),
                    pageable, directoryPage.getTotalElements()
            );
        }
        Page<Handbook> directoryPage = directoryRepository.findAll(pageable);
        return new PageImpl<HandbookDto>(
                directoryPage.getContent().stream()
                        .map(HandbookDto::from)
                        .collect(Collectors.toList()),
                pageable, directoryPage.getTotalElements()
        );
    }

    public Page<HandbookDto> findAll(Pageable pageable){
        Page<Handbook> directories = directoryRepository.findAll(pageable);
        return new PageImpl<HandbookDto>(
                directories.getContent().stream()
                        .map(HandbookDto::from)
                        .sorted(Comparator.comparing(HandbookDto::getArticle).reversed())
                        .collect(Collectors.toList()),
                pageable, directories.getTotalElements()
        );
    }

    public void add(DirectoryAddDto form){
        Handbook directory = Handbook.builder()
                .title(form.getTitle())
                .article(form.getArticle())
                .name(form.getName())
                .name2(form.getName2())
                .name3(form.getName3())
                .name4(form.getName4())
                .build();
        directoryRepository.save(directory);
    }

    public HandbookDto update(HandbookDto form){
        var directoryOptional = directoryRepository.findById(form.getId());
        if (directoryOptional.isEmpty()){
            return null;
        }
        Handbook directory = directoryOptional.get();
        directory.setArticle(form.getArticle());
        directory.setTitle(form.getTitle());
        directory.setName(form.getName());
        directory.setName2(form.getName2());
        directory.setName3(form.getName3());
        directory.setName4(form.getName4());
        return HandbookDto.from(directoryRepository.save(directory));
    }

    public HandbookDto findById(long id) {
        var directory = directoryRepository.findById(id).orElse(null);
        return HandbookDto.from(directory);
    }
}
