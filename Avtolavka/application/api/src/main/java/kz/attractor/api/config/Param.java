package kz.attractor.api.config;

public class Param {
    public static final double RATE_FIRST = 0.55;
    public static final double RATE_SECOND = 0.12;
    public static final double NDS = 0.12;
    public static final String EXCEL_TEMPLATE_PATH = "../prices/temp-excel-to-parse.xls";
    public static final String NO_IMAGE = "https://upload.wikimedia.org/wikipedia/commons/9/9a/%D0%9D%D0%B5%D1%82_%D1%84%D0%BE%D1%82%D0%BE.png";

}