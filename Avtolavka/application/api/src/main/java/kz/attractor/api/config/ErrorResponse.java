package kz.attractor.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private final String errorCode;
    private final String errorMessage;
    private final String detail;

    public ErrorResponse(Throwable t) {
        this.errorCode = t.getClass().getSimpleName();
        this.errorMessage = t.getMessage();
        this.detail = (t.getCause() != null) ? t.getCause().toString() : null;

    }
}
