package kz.attractor.api.controller.frontendController;

import kz.attractor.api.dto.ClientKbeAddDto;
import kz.attractor.api.dto.ClientKbeDto;
import kz.attractor.api.service.ClientKbeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ClientKbeController {
    private final ClientKbeService clientKbeService;

    @GetMapping("/clientKbes")
    public String showClients(Model model) {
        List<ClientKbeDto> clientKbeDtos = clientKbeService.findAll();
        model.addAttribute("page", clientKbeDtos);
        return "clients";
    }

    @GetMapping("/clientKbe-add")
    public String add(){return "clientKbe-add";}

    @PostMapping("/clientKbe-add")
    public String add(@Valid ClientKbeAddDto form,
                      BindingResult validationResult,
                      RedirectAttributes attributes){
        attributes.addFlashAttribute("form", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/clientKbe-add";
        }
        clientKbeService.add(form);
        return "redirect:/clients/add";
    }
}
