package kz.attractor.api.price.sender;

import kz.attractor.common.enums.SenderType;
import kz.attractor.emailclient.service.EmailClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;


@RequiredArgsConstructor
@Service
public class PriceSenderFactory {

    public static HashMap<SenderType, PriceSender<EmailClient>> priceSenders = new HashMap<>();


    static {
        priceSenders.put(SenderType.MAIL, new MailPriceSender());
    }

}
