package kz.attractor.api.price.creator;

import java.util.List;

public interface PriceCreator<T, S> {
    S create(List<T> t);
}
