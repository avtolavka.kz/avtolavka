package kz.attractor.api.service;

import kz.attractor.api.config.Param;
import kz.attractor.api.dto.ExcelParseProfileDto;
import kz.attractor.datamodel.model.*;
import kz.attractor.datamodel.repository.ExcelParseProfileRepository;
import kz.attractor.datamodel.repository.ProductNameRepository;
import kz.attractor.datamodel.repository.ProductRepository;
import kz.attractor.datamodel.repository.WarehouseRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;

@Slf4j
@Service
@AllArgsConstructor
public class ExcelParseService {
    private final ProductRepository productRepository;
    private final WarehouseRepository warehouseRepository;
    private final ProductNameRepository productNameRepository;
    private final ExcelParseProfileRepository excelParseProfileRepository;
    private final FileStoryService fileStoryService;

    public List<ExcelParseProfileDto> findAllExcelParseProfiles() {
        var profiles = excelParseProfileRepository.findAll()
                .stream()
                .map(ExcelParseProfileDto::from)
                .collect(Collectors.toList());
        return profiles;
    }

    public void parseExcel(MultipartFile multipartFile, long profileId) throws IOException {
        storeReceivedExcel(multipartFile);
        loadWorkbook(multipartFile, profileId);
    }

    private void loadWorkbook(MultipartFile multipartFile, long profileId) throws IOException{
        String fileName = multipartFile.getOriginalFilename();
        fileStoryService.add(fileName);
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        switch (extension) {
            case "xls":
                readExcelOld(profileId);
                break;
            case "xlsx":
                readExcelNew(profileId);
                break;
            default:
                throw new RuntimeException("Unknown Excel file extension: " + extension);
        }
    }

    private void readExcelOld(long profileId) {
        try (FileInputStream inputStream = new FileInputStream(new File(Param.EXCEL_TEMPLATE_PATH))) {
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            var sheet = workbook.sheetIterator();
            while (sheet.hasNext()){
                Iterator<Row> rowIterator = sheet.next().rowIterator();
                var profile = excelParseProfileRepository.findById(profileId).get();
                int count = 0;
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    parse(row, profile);
                    count++;
                }
                log.info("Данные из прайс листа записаны в БД: {} записей", count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readExcelNew(long profileId) {
        try (FileInputStream inputStream = new FileInputStream(new File(Param.EXCEL_TEMPLATE_PATH))) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            var sheet = workbook.sheetIterator();
            while (sheet.hasNext()){
                Iterator<Row> rowIterator = sheet.next().rowIterator();
                var profile = excelParseProfileRepository.findById(profileId).get();
                int count = 0;
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    parse(row, profile);
                    count++;
                }
                log.info("Данные из прайс листа записаны в БД: {} записей", count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parse(Row row, ExcelParseProfile profile) {
        if (row.getRowNum() < profile.getRowStart() - 1) {
            return;
        }

        DataFormatter formatter = new DataFormatter();
        String name = row.getCell(profile.getColName() - 1).getStringCellValue();
        String article = formatter.formatCellValue(row.getCell(3));
        String manufacturer = row.getCell(profile.getColManufacturer() -1).getStringCellValue();
        String articleSearch = article.replaceAll("[^\\w+]", "");
        String dateEdit = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));


        double price = row.getCell(profile.getColPrice() - 1).getNumericCellValue();
        int quantity = profile.getDefaultQuantity();
        if (row.getCell(profile.getColQuantity() - 1).getCellTypeEnum() == NUMERIC) {
            quantity = (int) row.getCell(profile.getColQuantity() - 1).getNumericCellValue();
        }

        String unit = "";
        if (profile.getColUnit() != -1) {
            unit = row.getCell(profile.getColUnit() - 1).getStringCellValue();
        }
        Product product = productRepository.getByName(name);

        if (product == null) {
            saveProducts(name, article, articleSearch, price, manufacturer, quantity, unit, dateEdit, profile.getWarehouse());
        }
        else if (product.getName().equalsIgnoreCase(name)){
            editProduct(product.getId(), price, quantity, dateEdit);
        }
    }

    private void saveProducts(String name, String article, String articleSearch,
                              double price, String manufacturer, int quantity,
                              String unit, String dateEdit, Warehouse warehouse) {
        ProductName productName = ProductName.builder()
                .nameProduct(name)
                .build();
        try{
            productNameRepository.save(productName);

        }catch (Exception e){
            log.info(productName.getNameProduct());
        }


        Product product = Product.builder()
                .name(name)
                .article(article)
                .articleSearch(articleSearch)
                .quantity(quantity)
                .dateEdit(dateEdit)
                .manufacturer(manufacturer)
                .image(Param.NO_IMAGE)
                .purchasePrice(BigDecimal.valueOf(price))
                .sellingPrice(BigDecimal.valueOf(price * (Param.RATE_FIRST + 1) * (Param.RATE_SECOND + 1)))
                .unit(unit)
                .quality(ProductQuality.NEW)
                .status(ProductStatus.NOT_AVAILABLE)
                .productName(productName)
                .warehouse(warehouse)
                .build();
        productRepository.save(product);
    }


    public void editProduct(int id, double price, int quantity, String dateEdit){
        Product product = productRepository.findById(id).get();
        product.setPurchasePrice(BigDecimal.valueOf(price));
        product.setSellingPrice(BigDecimal.valueOf(price * (Param.RATE_FIRST + 1) * (Param.RATE_SECOND + 1)));
        product.setQuantity(quantity);
        product.setDateEdit(dateEdit);
        productRepository.save(product);
        log.info("Товар {} скоректирован.", product.getName());
    }

    private void storeReceivedExcel(MultipartFile multipartFile) {
        File file = new File(Param.EXCEL_TEMPLATE_PATH);

        try (OutputStream os = new FileOutputStream(file)) {
            os.write(multipartFile.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
