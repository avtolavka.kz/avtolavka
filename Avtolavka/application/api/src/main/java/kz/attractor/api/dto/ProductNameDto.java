package kz.attractor.api.dto;

import kz.attractor.datamodel.model.ProductName;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductNameDto {

    private long id;
    private String nameProduct;
    private String nameProduct2;
    private String nameProduct3;
    private String nameProduct4;

    public static ProductNameDto from(ProductName productName){
        return ProductNameDto.builder()
                .id(productName.getId())
                .nameProduct(productName.getNameProduct())
                .nameProduct2(productName.getNameProduct2())
                .nameProduct3(productName.getNameProduct3())
                .nameProduct4(productName.getNameProduct4())
                .build();

    }
}
