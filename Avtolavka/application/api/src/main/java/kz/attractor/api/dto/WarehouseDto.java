package kz.attractor.api.dto;

import kz.attractor.datamodel.model.Warehouse;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseDto {

    private Long id;
    private String name;
    private boolean includeToPriceList;
    private String deliveryTime;
    private String shortName;

    public static WarehouseDto from(Warehouse warehouse){
        return WarehouseDto.builder()
                .id(warehouse.getId())
                .name(warehouse.getName())
                .shortName(warehouse.getShortName())
                .includeToPriceList(warehouse.isIncludeToPriceList())
                .deliveryTime(warehouse.getDeliveryTime())
                .build();

    }
}
