$(document).ready(function (){

    function handleProductDetails(response) {
        $('#productDetailsModalTitle').empty()
        $('#productDetailsModal').find('.modal-body').empty()
        $('#productDetailsModalTitle').text(`Товар №${response.id}`)
        let productDetails = $(`
            <p>Название: ${response.name}</p>
            <p>Альтернативное название: ${response.productName.nameProduct}</p>
            <p>Артикль: ${response.article}</p>
            <p>Цена: ${response.price}</p>
            <p>Количество: ${response.quantity}</p>
            <p>Примечание: ${response.status}</p>
            <p>Склад: ${response.warehouse.name}</p>
            <div>
                <p>Теги: ${response.tag}</p>
                <button id="add_tags_btn" class="btn btn-primary">Добавить теги</button>
            </div>
            <div>
            <a class="btn btn-primary" href="/product/${response.id}/edit">Редактировать  товар</a>
            </div>
            <div>
            <form action="remove" method="post">
               <button class="btn btn-primary">Удалить товар</button>   
            </form> 
            </div>
             <form class="tags_form">
                <p>Добавить теги:</p>
                <input class="form-control my-2" type="text" placeholder="Введите нужные вам теги" required>
                <button type="submit" class="btn btn-primary">Добавить теги</button>
            </form>
           `)
        $('#productDetailsModal').find('.modal-body').append(productDetails)
        $('#add_tags_btn').click(function (){
            $('#add_tags_btn').hide()
            $('.tags_form').show()
        })
        $('.tags_form').submit(function (e){
            e.preventDefault()

            $.ajax({
                method: "POST",
                url: window.location.origin + "/api/products/tag",
                data: {
                    productId: response.id,
                    tag: $('.tags_form').find('input').val()
                },
                success: (response2) => {
                    $.ajax({
                        method: "GET",
                        url: window.location.origin + "/api/products/"+response.id,
                        success: (response3) => {
                            handleProductDetails(response3)
                        },
                        error: (error) => {
                            console.log(error)
                        }
                    })
                },
                error: (error) => {
                    console.log(error)
                }
            })
        })
    }

    $('.product_tr').click(function (){
        let productId = $(this).find('.product_tr_id').text()
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/products/"+productId,
            success: (response) => {
                handleProductDetails(response)
            },
            error: (error) => {
                console.log(error)
            }
        })
    })
})