'use strict';
const server = window.location.origin;
const serverApi = server + "/api/";
let contactId = $('#containerContactEdit').attr('data-contact-id');

requestContactInfo();

function requestContactInfo() {
    $.ajax({
        method: "GET",
        url: serverApi + "contacts/" + contactId,
        success: (response) => {
            mainContactEdit(response);
        },
        error: (error) => {
            console.log(error);
        }
    })
}

function mainContactEdit(response) {
    showPageLayout(response);
    setCurrentStatus(response.status);
    setHandlers(response);
}

function setCurrentStatus(status) {
    $('option').each( function(index, element) {
        if ($(element).val() === status) {
            $(element).attr('selected', true);
        }
    });
}

function setHandlers(response) {
    selectMainPhone();
    selectMainEmail();
    sendEditContactRequest(response.clientId);
}

function sendEditContactRequest(clientId) {
    $('#editContactForm').submit(function(e) {
        e.preventDefault();
        e.stopPropagation();
        if ($('#name').val() == "") {
            alert('Заполните имя');
            return;
        }
        console.log($('#editContactForm').serialize());
        $.ajax({
            method: "PUT",
            url: serverApi + "contacts/" + contactId,
            data: $('#editContactForm').serialize(),
            success: (response) => {
                window.location = window.location.origin + "/clients/" + clientId;
            },
            error: (error) => {
                alert(error)
            }
        })
    })
}

function selectMainPhone() {
    $('.phoneStatus').click(function () {
        $('.phoneMain').val($(this).parent().find('input').val());
    })
}

function selectMainEmail() {
    $('.emailStatus').click(function () {
        $('.emailMain').val($(this).parent().find('input').val());
    })
}

function showPageLayout(contact) {
    let html = $(`
        <h1 class="my-3">Редактирование карточки контактного лица</h1>
        <a class="btn btn-primary" href="/clients/${contact.clientId}" style="background-color: RGB(215, 181, 109); color: #4f5050;">Отменить редактирование</a>
        <br>
        <form id="editContactForm">
            <input type="text" name="clientId" value="${contact.clientId}" hidden>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Полное имя:</span>
                </div>
                <div class="col-9">
                    <input value="${contact.name}" id="name" type="text" name="name" style="width: 100%">
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Должность:</span>
                </div>
                <div class="col-9">
                    <input value="${contact.position}" id="position" type="text" name="position" style="width: 100%">
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">День рождения:</span>
                </div>
                <div class="col-9">
                    <input value="${contact.dateOfBirth}" id="dateOfBirth" type="text" name="dateOfBirth" style="width: 100%">
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Основной Телефон:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.phoneMain}" class="phoneMain" type="text" name="phoneMainDisabled" style="width: 100%" disabled>
                    <input value="${contact.phoneMain}" class="phoneMain" type="text" name="phoneMain" hidden>
                </div>
            </div>
            <div class="row mt-2 phone1">
                <div class="col-3">
                    <span class="label-input-text mb-1">Телефон 1:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.phone1}" type="text" name="phone1" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary phoneStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2 phone2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Телефон 2:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.phone2}" type="text" name="phone2" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary phoneStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2 phone3">
                <div class="col-3">
                    <span class="label-input-text mb-1">Телефон 3:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.phone3}" type="text" name="phone3" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary phoneStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Основной e-mail:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.emailMain}" class="emailMain" type="text" name="emailMainDisabled" style="width: 100%" disabled>
                    <input value="${contact.emailMain}" class="emailMain" type="text" name="emailMain" hidden>
                </div>
            </div>
            <div class="row mt-2 email1">
                <div class="col-3">
                    <span class="label-input-text mb-1">E-mail 1:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.email1}" type="text" name="email1" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary emailStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2 email2">
                <div class="col-3">
                    <span class="label-input-text mb-1">E-mail 2:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.email2}" type="text" name="email2" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary emailStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2 email3">
                <div class="col-3">
                    <span class="label-input-text mb-1">E-mail 3:</span>
                </div>
                <div class="col-3">
                    <input value="${contact.email3}" type="text" name="email3" style="width: 100%">
                </div>
                <div class="col-2 mx-5 btn btn-primary emailStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">
                    <span>Сделать основным</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-3">
                    <span class="label-input-text mb-1">Статус:</span>
                </div>
                <div class="col-3">
                    <select name="status">
                        <option value="" hidden>Выберите статус</option>
                        <option value="Постоянный">Постоянный</option>
                        <option value="Новый">Новый</option>
                        <option value="Архивный">Архивный</option>
                    </select>
                </div>
            </div>
            <div class="mt-3">
                <button class="btn btn-primary" type="submit" style="background-color: RGB(215, 181, 109); color: #4f5050;">Сохранить</button>
            </div>
        </form>
        
    `)
    $('#containerContactEdit').append(html)
}