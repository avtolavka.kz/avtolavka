$(document).ready(function () {
    getLibraries();

    function getLibraries(query = null) {
        let url;
        if (query != null) {
            let path = window.location.origin + '/api/libraries'
            url = path + '?query=' + query
            $.ajax({
                method: "GET",
                url: url,
                success: (url) => {
                    handleLibraries(url);
                },
                error: (error) => {
                    alert(error)
                }
            })
        }

    }
    $('#find_libraries_form').submit(function (e){
        e.preventDefault()
        let query = $('#librariesInput').val();
        getLibraries(query)
    })

    function handleLibraries(response) {
        $('.order_add_right_libraries').empty()
        let order_products = $('.order_add_left_libraries').children()
        response.map(library => {
            let library_item = $(`
                <tr class="library_row_item">
                     <th class="library_tr_id" scope="row" hidden>${library.id}</th>-->
                        <td>${library.title}</td>
                        <td>${library.date}</td>
                </tr>
            `)
            $('.order_add_right_libraries').append(libraries_item)
        })

    }
})