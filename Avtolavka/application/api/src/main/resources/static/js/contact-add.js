'use strict';
const server = window.location.origin;
const serverApi = server + "/api/";
setHandlers();

function setHandlers() {
    showPhone2();
    showPhone3();
    selectMainPhone();
    showEmail2();
    showEmail3();
    selectMainEmail();
    // sendAddClientRequest();
}
function showPhone2() {
    $('.phone1').find('.btnPhoneAdd').click(function () {
        $('.phone2').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function showPhone3() {
    $('.phone2').find('.btnPhoneAdd').click(function () {
        $('.phone3').removeAttr('hidden');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $(this).empty();
    })
}

function selectMainPhone() {
    $('.phoneStatus').click(function () {
        $('.phoneStatus').find('span').text('Сделать основным');
        $('.phoneStatus').addClass('btn-xs');
        $('.phoneStatus').addClass('rounded-1');
        $('.phoneStatus').addClass('btn-primary');
        $(this).find('span').text('Основной');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $('.phoneStatus').parent().find('input').removeClass('phoneMain');
        $(this).parent().find('input').addClass('phoneMain');
    })
}

function showEmail2() {
    $('.email1').find('.btnEmailAdd').click(function () {
        $('.email2').removeAttr('hidden');
        $(this).remove();
    })
}

function showEmail3() {
    $('.email2').find('.btnEmailAdd').click(function () {
        $('.email3').removeAttr('hidden');
        $(this).remove();
    })
}

function selectMainEmail() {
    $('.emailStatus').click(function () {
        $('.emailStatus').find('span').text('Сделать основным');
        $('.emailStatus').addClass('btn-xs');
        $('.emailStatus').addClass('rounded-1');
        $('.emailStatus').addClass('btn-primary');
        $(this).find('span').text('Основной');
        $(this).removeClass('btn-xs');
        $(this).removeClass('rounded-1');
        $(this).removeClass('btn-primary');
        $('.emailStatus').parent().find('input').removeClass('emailMain');
        $(this).parent().find('input').addClass('emailMain');
    })
}