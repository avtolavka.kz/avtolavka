$(document).ready(function (){
    function handleProductDetails(response) {
        $('#directoryDetailsModalTitle').empty()
        $('#directoryDetailsModal').find('.modal-body').empty()
        $('#directoryDetailsModalTitle').text(`Товар №${response.id}`)
        let productDetails = $(`
            <p style="font-weight: bold">Основное наименование: ${response.title}</p>
            <p>Артикль: ${response.article}</p>
            <p>Дополнительное наименование: ${response.name}</p>
            <p>Дополнительное наименование 2: ${response.name2}</p>
            <p>Дополнительное наименование 3: ${response.name3}</p>
            <p>Дополнительное наименование 4: ${response.name4}</p>
           `)
        $('#directoryDetailsModal').find('.modal-body').append(productDetails)
    }

    $('.directory_tr').click(function (){
        let productId = $(this).find('.directory_tr_id').text()
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/directories/"+productId,
            success: (response) => {
                handleProductDetails(response)
            },
            error: (error) => {
                console.log(error)
            }
        })
    })

})