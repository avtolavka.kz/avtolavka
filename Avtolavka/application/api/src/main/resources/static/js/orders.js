$(document).ready(function (){
addSuborder();
    function handleOrderDetails(response) {
        $('#orderDetailsModalTitle').empty()
        $('#orderDetailsModal').find('.modal-body').empty()
        $('#orderDetailsModalTitle').text(`${response.order.id}`)
        let orderDetails = $(`
        <div class="row">
            <div class="col-6">
                <p>Поставщик: ${response.order.company.name}</p>
            </div>
            <div class="col-3">
                 <a class="btn" href="/orders/${response.order.id}/download">Скачать в Excel</a>
            </div>
            <div class="col-3">
                 <a class="btn" href="/orders/${response.order.id}/suborder">Выставить КП от другого поставщика</a>
            </div>
        </div>
            
            <p>Имя менеджера: ${response.order.client.name} (${response.order.client.name})</p>
<!--            <p>Дата создания: ${response.order.dateCreation}</p>-->
            <p>Дата создания: ${response.orderCreateDate}</p>
            <p>Товары:</p>
            <p id="orderId" data-order-id="${response.order.id}" hidden></p>
            <table id="modal_products_table" class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Единица измерения</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Цена продажи</th>
                        <th scope="col">Примечание</th>
                        <th scope="col">Изображение</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <div id="order_sum"></div>`)
        let sum = 0.0
        let idProducts = 0;
        for (let i = 0; i < response.orderProducts.length; i++) {
            idProducts++;
            $(function (){
                for (let i=0; i< response.orderProducts.length; i++){
                    if(response.orderProducts[i].price < response.orderProducts[i].product.sellingPrice){
                        $('.price_input' ).css('color', 'red');
                    }
                }
            })

            orderDetails.find('tbody').append($(`
            <tr>
                <th class="order_product_item_id" scope="row">${idProducts}</th>
                <td>${response.orderProducts[i].product.name}</td>
                <td class="quantity_input">${response.orderProducts[i].quantity}</td>
                <td class="unit_input">${response.orderProducts[i].unit}</td>
                <td>${response.orderProducts[i].product.sellingPrice}</td>
                <td class="price_input" id="price">${response.orderProducts[i].price}</td>
                <td>${response.orderProducts[i].product.inStock?"В наличии":"Под заказ"}</td>
                <td><img src="${response.orderProducts[i].product.image}" width="100" height="100"></td>
            </tr>`))
            sum += response.orderProducts[i].quantity * response.orderProducts[i].price;
            sumNDS = sum * 0.12;
        }
        if (!response.order.closed) {
            $('#orderDetailsModalCloseForm').show()
            $('#closingOrderId').attr('value', response.order.id)
        } else {
            $('#orderDetailsModalCloseForm').hide()
            $('#add_account_btn').hide()
        }
        $('#orderDetailsModal').find('.modal-body').append(orderDetails)
        $('#order_sum').append($(`<p>Итого сумма с НДС: ${Math.round(sum)} </p>
                                                    <p>Из них НДС 12%: ${Math.round(sumNDS)} </p>`))
        $('.bonus_container').empty()
        if(!response.order.hasBonus) {
            $('#bonus_btn').attr('data-value', true)
            $('#bonus_btn').removeClass('btn-warning')
            $('#bonus_btn').addClass('btn-success')
            $('#bonus_btn').text('Вернуть бонусы(12%)')
        } else {
            $('#bonus_btn').attr('data-value', false)
            $('#bonus_btn').removeClass('btn-success')
            $('#bonus_btn').addClass('btn-warning')
            $('#bonus_btn').text('Убрать бонусы(12%)')
        }

    }

    function addSuborder() {
        $('.col-3').find('#makeKP').click(function () {
            $('.suborder').removeAttr('hidden');
            $(this).empty();
        })
    }
    function handleAddAccount(option = true) {
        if (option) {
            $('.isBorrow').show()
            $('#add_account_btn').hide()
            $('#make_account_btn').show()
            $('#modal_products_table').find('tbody tr').append($('<td><input checked class="form-check-input check_product" type="checkbox"></td>'))
            $('.check_product').click(function (){
                let products = $('#modal_products_table tbody').children()
                let sum = 0.0;
                for(let i = 0; i < products.length; i++) {
                    if ($(products[i]).find('.check_product').is(':checked')) {
                        sum += parseFloat($(products[i]).find('.quantity_input').text()) * parseFloat($(products[i]).find('.price_input').text())
                    }
                }
                $('#order_sum').empty()
                $('#order_sum').append($(`<p>Итого: ${Math.round(sum)} </p>`))
            })
        } else  {
            $('.isBorrow').hide()
            $('#add_account_btn').show()
            $('#make_account_btn').hide()
            $('#modal_products_table').find('tbody tr').remove($('.check_product'))
        }
    }

    function handleInvoiceDetails(response) {
        $('#invoiceDetailsModalTitle').empty()
        $('#invoiceDetailsModal').find('.modal-body').empty()
        $('#invoiceDetailsModalTitle').text(`${response.invoice.id} для коммерческого предложения №${response.invoice.order.id}`)
        let invoiceDetails = $(`
            <p>Имя менеджера: ${response.invoice.order.contact.name} (${response.invoice.order.contact.client.name})</p>
            <p>Дата создания счета: ${response.invoice.createdDate}</p>
            <div class="row mt-3">
                <div class="col-6">
                  <p>Статус оплаты: ${response.invoice.accountPaymentStatus.name}</p>
                </div> 
                <div class="col-6">
                    <a class="btn btn-primary" href="/orders/${response.invoice.order.id}/editPaymentStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">Сменить статус оплаты</a>
                </div>
            </div>
              <div class="row mt-3">
                <div class="col-6">
                     <p>Статус доставки: ${response.invoice.supplyStatus.name}</p>
                </div> 
           
                <div class="col-6">
                      <a class="btn btn-primary" href="/orders/${response.invoice.order.id}/editSupplyStatus" style="background-color: RGB(215, 181, 109); color: #4f5050;">Сменить статус поставки</a>
                </div>
              </div>
              <div class="row mt-3">
              <div class="col-2"><p>Доверенность:</p></div>
                    <div class="col-4">
                                    <p><a href="/files/${response.invoice.file}" target="_blank">${response.invoice.file}</a></p> 
                    </div>
                  <div class="col-6 contract">
                          <a class="btn btn-primary" href="/orders/${response.invoice.id}/contract-add" id="contractAdd" style="background-color: RGB(215, 181, 109); color: #4f5050;">Загрузить доверенность</a>
                  </div>
              </div>
              
            <p>Товары:</p>
            <p id="invoiceId" data-invoice-id="${response.invoice.id}" hidden></p>
            <table id="modal_products_table" class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Единица измерения</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Цена продажи</th>
                        <th scope="col">Примечание</th>
                        <th scope="col">Сумма</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <div id="invoice_sum"></div>`)
        for (let i = 0; i < response.orderProducts.length; i++) {
            invoiceDetails.find('tbody').append($(`
            <tr>
                <th class="order_product_item_id" scope="row">${response.orderProducts[i].id}</th>
                <td>${response.orderProducts[i].product.name}</td>
                <td class="quantity_input">${response.orderProducts[i].quantity}</td>
                <td class="unit_input">${response.orderProducts[i].unit}</td>
                <td class="unit_input">${response.orderProducts[i].product.sellingPrice}</td>
                <td class="price_input">${response.orderProducts[i].price}</td>
                <td>${response.orderProducts[i].product.inStock?"В наличии":"Под заказ"}</td>
                
                <td>${response.orderProducts[i].quantity * response.orderProducts[i].price}</td>
            </tr>`))
        }
        $('#invoiceDetailsModal').find('.modal-body').append(invoiceDetails);
        let sum = response.invoice.sum;
        let sumNDS =sum * 0.12;
        $('#invoice_sum').append($(`<p>Итого сумма с НДС: ${Math.round(sum)} тенге </p>
                                                    <p>Из них НДС 12%: ${Math.round(sumNDS)} тенге </p>`))
        $('#contractAdd').click(function (){
            $('.contractFile').show()
        })
    }

    $('.order_card').click(function (){
        let orderId = $(this).find('.order_card_id').text()
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/orders/"+orderId,
            success: (response) => {
                handleOrderDetails(response)
            },
            error: (error) => {
                console.log(error)
            }
        })
    })
    $('.invoice_card').click(function (){
        let invoiceId = $(this).find('.invoice_card_id').text();
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/invoices/" + invoiceId,
            success: (response) => {
                handleInvoiceDetails(response)
            },
            error: (error) => {
                console.log(error);
            }
        })
    })

    $('#bonus_btn').click(function (){
        handleAddAccount(false)
        let isTrue = ($(this).attr('data-value') === 'true')
        let id = $('#orderDetailsModalTitle').text()
        console.log(isTrue)
        console.log(id)
        $.ajax({
            method: "POST",
            url: window.location.origin + "/api/orders/bonus",
            data: {
                orderId: id,
                bonus: isTrue
            },
            success: (response) => {
                $.ajax({
                    method: "GET",
                    url: window.location.origin + "/api/orders/"+id,
                    success: (response) => {
                        handleOrderDetails(response)
                    },
                    error: (error) => {
                        console.log(error)
                    }
                })
            },
            error: (error) => {
                alert(error)
            }
        })
    })
    $('#actionSendAsHtml').click(function () {
        let orderId = $('#orderId').attr('data-order-id');
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/email/order/"+ orderId,
            success: (response) => {
                alert("Письмо отправлено на " + response.to);
            },
            error: (error) => {
                console.log(error)
            }
        })
    })
    $('#actionSendAsExcel').click(function () {
        let orderId = $('#orderId').attr('data-order-id');
        console.log(orderId)
        $.ajax({
            method: "GET",
            url: window.location.origin + "/api/orders/download/" + orderId,
            success: (response) => {
                alert("Файл загружен ");
            },
            error: (error) => {
                console.log(error)
            }
        })
    })

    $('#make_account_btn').click(function (){
        let products = $('#modal_products_table tbody').children()
        let productsArr = [];
        let isEmpty = true
        for(let i = 0; i < products.length; i++) {
            if ($(products[i]).find('.check_product').is(':checked')) {
                isEmpty = false
                productsArr.push({
                    id: $(products[i]).find('.order_product_item_id').text(),
                    quantity: $(products[i]).find('.quantity_input').text(),
                    unit: $(products[i]).find('.unit_input').text()
                })
            }
        }
        if (isEmpty) {
            alert("Выберите минимум 1 товар для создания счета!")
            return
        }
        $.ajax({
            method: "POST",
            url: window.location.origin + "/api/orders/edit",
            contentType: "application/json",
            data: JSON.stringify({
                orderId:  $('#orderDetailsModalTitle').text(),
                products: productsArr
            }),
            success: (response) => {
                $.ajax({
                    method: "POST",
                    url: window.location.origin + "/api/accounts/add",
                    data: {
                        orderId:  $('#orderDetailsModalTitle').text(),
                        borrow: $('#isBorrow').is(':checked')
                    },
                    success: (response1) => {
                        window.location = window.location.origin + "/orders";
                    },
                    error: (error) => {
                        alert(error)
                    }
                })
            },
            error: (error) => {
                alert(error)
            }
        })


    })

    $('#orderDetailsModal').on('hidden.bs.modal', function (e) {
        handleAddAccount(false)
    })

    $('#add_account_btn').click(function () {
        handleAddAccount(true)
    })
})