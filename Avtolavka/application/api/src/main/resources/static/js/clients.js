'use strict';
$(document).ready(function (){
    const server = window.location.origin + "/api/";
    let messagingClientIds = [];
    makeRequest(0);

    function setHandlers() {
        $('.page-item').click(function (){
            let pageNumber = $(this).find('.page-link').text();
            $('.page-item').removeClass('active');
            $(this).addClass('active');
            makeRequest(pageNumber - 1);
        })
    }
    function getClients(query = null) {
        let url;
        if (query != null) {
            let path = window.location.origin + '/api/clients/search'
            url = path + '?query=' + query
            $.ajax({
                method: "GET",
                url: url,
                success: (url) => {
                    showClientsTable(url);
                },
                error: (error) => {
                    alert(error)
                }
            })
        }

    }
    $('#find_client_form').submit(function (e){
        e.preventDefault()
        let query = $('#clientInput').val();
        getClients(query)
    })


    function makeRequest(page) {
        $.ajax({
            method: "GET",
            url: server + "clients?page=" + page,
            success: (response) => {
                showPageLayout();
                // showPageNavigator(response.totalPages, response.number);
                showClientsTable(response.content);
                // setHandlers();
            },
            error: (error) => {
                console.log(error);
            }
        })
    }

    function showPageNavigator(totalPages, number) {
        for (let i = 1; i <= totalPages; i++) {
            if (number == i - 1) {
                $('.pagination').append($(`
                <li class="page-item active">
                    <span class="page-link">${i}</span>
                </li>
            `));
            } else {
                $('.pagination').append($(`
                <li class="page-item">
                    <span class="page-link">${i}</span>
                </li>
            `));
            }
        }
    }

    function showPageLayout() {
        let html = $(`
        <div class="container mb-3">
            <ul class="pagination justify-content-center"></ul>
            <table class="table table-hover mt-3">
                <thead class="table-light">
                <tr>
                    <th></th>
                    <th>Id клиента</th>
                    <th>Наименование</th>
                    <th>Краткое наименовние</th>
                    <th>Адрес</th>
                    <th>Дата создания</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody style="background-color: rgba(108, 117, 125, .5)"></tbody>
            </table>
    `)
        $('#containerClients').empty();
        $('#containerClients').append(html);
    }

    function showClientsTable(clients) {
        $('tbody').empty();
        for (let i = 0; i < clients.length; i++) {
            let inputChecked = $(`<input type="checkbox" value="${clients[i].id}" class="form-check-input client_id_checkbox" checked />`)
            messagingClientIds.push(clients[i].id);
            let elem = $(`
            <tr class="position-relative">
                <td class="check_field"></td>
                <td>${clients[i].id}</td>
                <td>
                    <a href="/clients/${clients[i].id}" class="stretched-link" style="text-decoration: none; color: black">
                        ${clients[i].name}
                    </a>
                </td>
                <td>${clients[i].shortName}</td>
                <td>${clients[i].address.legal_city}</td>
                <td>${clients[i].createDate}</td>
                <td>${clients[i].status}</td>
            </tr>`)
            elem.find('.check_field').append(inputChecked)
            $('tbody').append(elem);
        }
        if($('#price_btn').attr('data-state') == 'choice') {
            $('.client_id_checkbox').show()
        }
        $('.stretched-link').click(function (e){
            if($('#price_btn').attr('data-state') == 'choice') {
                e.preventDefault()
                if($(this).parent().parent().find('input').attr('checked')) {
                    $(this).parent().parent().find('input').attr('checked', false)
                     const index = messagingClientIds.indexOf($(this).parent().parent().find('input').val())
                     messagingClientIds.splice(index, 1)
                } else {
                    $(this).parent().parent().find('input').attr('checked', true)
                    messagingClientIds.push($(this).parent().parent().find('input').val())
                }
            }
        })
    }

    $('#price_btn').click(function (){
        if($(this).attr('data-state') == 'btn') {
            $(this).attr('data-state', 'choice')
            $(this).text('Отправить прайс')
            $('.client_id_checkbox').show()
        } else {
            let data = JSON.stringify(messagingClientIds);
            $.ajax({
                method: "POST",
                url: server + "email/clients/send_price",
                contentType: 'application/json',
                data : data,
                success: (response) => {
                    window.location = 'http://localhost:8500/clients'
                },
                error: (error) => {
                    alert(error);
                }
            })
        }
    })
})
