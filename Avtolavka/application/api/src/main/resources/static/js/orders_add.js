$(document).ready(function (){

    function handleClients(response) {
        $('.client_names_wrapper').empty()
        if (response.length != 0) {
            $('.client_names_wrapper').append($('<h5 class="mb-2">Выберите клиента:</h5>'))
            response.map(i => {
                let client_item = $(`<div class="client_item">
                    <input type="hidden" value="${i.id}"/>
                    <h5>${i.name}</h5>
                    <h6>${i.clientName}</h6>
                </div>`)
                client_item.click(function (){
                    $('#add_order_btn').attr('disabled', false)
                    $('.client_names_wrapper').empty()
                    $('#client_name').text(client_item.find('h5').text() + `(${i.clientName})`)
                    $('#client_name').append($(`<input type="hidden" value="${client_item.find('input').val()}" name="client_id" id="chosen_client_id" />`))
                })
                $('.client_names_wrapper').append(client_item)
            })
        } else {
            $('.client_names_wrapper').append($('<div class="alert alert-primary" role="alert">Клиенты не найдены</div>'))
        }
    }

    $('#find_client_form').submit(function (e){
        e.preventDefault()
        let query = $('#clientInput').val();
        $.ajax({
            method: "GET",
            url: window.location.origin + `/api/clients/search?query=${query}`,
            success: (response) => {
                handleClients(response)
            },
            error: (error) => {
                alert(error)
            }
        })
    })

    function onProductItemRowClick() {
        $('.product_row_item').click(function (){
            if (!$(this).find('input').attr('checked')) {
                let product_item = $(`
               <tr>
                    <td class="order_product_item_id">${$(this).find('.product_row_item_id').text()}</td>
                    <td>${$(this).find('.product_row_item_name').text()}</td>
                    <td>${$(this).find('.product_row_item_priceProduct').text()}</td>
                    <td><input class="order_product_item_input form-control price_input" type="number" value="${$(this).find('.product_row_item_priceProduct').text()}"></td>
                    <td>${$(this).find('.product_row_item_quantity').text()}"</td>
                    <td><input class="order_product_item_input form-control quantity_input" type="number" value="${$(this).find('.product_row_item_quantityKP').text()}"></td>
                    <td><input class="order_product_item_input form-control unit-class" type="text" value="${$(this).find('.product_row_item_unit').text()}"></td>
                    <td><button class="delete_order_product_btn btn btn-danger"><span>&times;</span></button></td>
               </tr> 
            `)
                product_item.find('.delete_order_product_btn').click(function (){
                    let order_products = $('.order_add_right_products').children()
                    if (order_products.length > 0) {
                        for (let i = 0; i < order_products.length; i++) {
                            if ($(order_products[i]).find('.product_row_item_id').text() == product_item.find('.order_product_item_id').text()) {
                                $(order_products[i]).find('input').attr('checked', false)
                            }
                        }
                    }
                    product_item.remove()
                })
                $('.order_add_left_products').append(product_item)
                $(this).find('input').attr('checked', true)
            } else {
                let order_products = $('.order_add_left_products').children()
                for (let i = 0; i < order_products.length; i++) {
                    if ($(order_products[i]).find('.order_product_item_id').text() == $(this).find('.product_row_item_id').text()) {
                        $(order_products[i]).remove()
                    }
                }
                $(this).find('input').attr('checked', false)
            }
        })
    }
    function handleProducts(response) {
        $('.order_add_right_products').empty()
        let order_products = $('.order_add_left_products').children()
        response.map(product => {
            let product_item = $(`    
                <tr class="product_row_item">
                    <td class="product_row_item_id" hidden>${product.id}</td>
                    <td><p style="font-weight: bold">${product.manufacturer}</p>
                    <p style="color: #0a53be; line-height: 15px; width: 200px; box-sizing: border-box; overflow: hidden; text-overflow: ellipsis; white-space: nowrap">${product.article}</p></td>
                    <td class="product_row_item_name">${product.name}</td>
                    <td class="product_row_item_priceProduct" style="color: red">${product.price}</td>
                    <td class="product_row_item_quantity">${product.quantity}</td>
                    <td class="product_row_item_quantityKP" hidden>${product.quantity = 1}</td>
                    <td class="product_row_item_unit">${product.unit}</td>
                    <td>${product.warehouse.deliveryTime}</td>
                    <td><input type="checkbox" value="${product.id}" class="form-check-input product_checkbox"></td>
                </tr>
            `)
            if (order_products.length > 0) {
                for (let i = 0; i < order_products.length; i++) {
                    if ($(order_products[i]).find('.order_product_item_id').text() == product_item.find('.product_row_item_id').text()) {
                        product_item.find('input').attr('checked', true)
                    }
                }
            }
            $('.order_add_right_products').append(product_item)
        })
        onProductItemRowClick()
    }



    function getProducts(query = null) {
        let url;
        if (query != null) {
            let path = window.location.origin + '/api/products/search'
            url = path + '?query=' + query
        }
        $.ajax({
            method: "GET",
            url: url,
            success: (url) => {
                console.log(url)
                handleProducts(url)
            },
            error: (error) => {
                alert(error)
            }
        })
    }

    getProducts()

    $('#find_product_form').submit(function (e){
        e.preventDefault()
        let query = $('#productInput').val();
        getProducts(query)
    })

    $('.add_order_form').submit(function (e){
        e.preventDefault()
        let companyId = $('#companyId').val()
        let clientId = $('#client_name').find('input').val()
        let products = $('.order_add_left_products').children()
        if (products.length != 0) {
            let productsArr = []
            for(let i = 0; i < products.length; i++) {
                productsArr.push({
                    id: $(products[i]).find('.order_product_item_id').text(),
                    price: $(products[i]).find('.price_input').val(),
                    quantity: $(products[i]).find('.quantity_input').val(),
                    unit:  $(products[i]).find('.unit-class').val()
                })
            }

            $.ajax({
                method: "POST",
                url: window.location.origin + '/api/orders/add',
                contentType: "application/json",
                data: JSON.stringify({
                    companyId: companyId,
                    contactId: clientId,
                    products: productsArr
                }),
                success: (response) => {
                    window.location = window.location.origin + "/orders";
                },
                error: (error) => {
                    alert(error)
                }
            })
        } else {
            alert("Добавьте минимум 1 продукт чтобы сохранить КП.")
        }
    })
    function requestCompanies(companies) {
        $.ajax({
            method: "GET",
            url: serverApi + "companies",
            success: (response) => {
                console.log(response)
                main(response)
            },
            error: (error) => {
                console.log(error);
            }
        })
    }
    function company(companies) {
        for (let i = 0; i < companies.length; i++) {
            $('#companyId').append($(`
            <option value="${companies[i].id}">${companies[i].name}</option>
        `));
        }
    }
})